//
//  TabBarViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 16/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    var loadingView: UIView!
    var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBar.appearance().tintColor = UIColor(red: 0.4902, green: 0.8627, blue: 0.2902, alpha: 1)
        UINavigationBar.appearance().tintColor = UIColor(red: 0.4902, green: 0.8627, blue: 0.2902, alpha: 1)
        UISegmentedControl.appearance().tintColor = UIColor(red: 0.4902, green: 0.8627, blue: 0.2902, alpha: 1)
        UINavigationBar.appearance().backgroundColor = UIColor(red: 0.9686, green: 0.9725, blue: 0.9765, alpha: 1.0)
        
        
        self.addLoadingView()
        self.loadAppInfo()
    }
    
    func loadAppInfo() {
        self.indicator.startAnimating()
        
        NetworkHelper.instance.prepareApp(success: {
            self.loadUserInfo()
        }, failure: {
            self.loadUserInfo()
        })
    }
    
    func loadUserInfo() {
        NetworkHelper.instance.getUserInfo(success: { isLoggedIn in
            self.loadRestaurants()
        }, failure: { error in
            self.loadRestaurants()
        })
    }
    
    func loadRestaurants() {
        NetworkHelper.instance.loadRestaurants {
            self.removeLoadingView()
        }
    }
    
    func removeLoadingView() {
        
        indicator.stopAnimating()
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.loadingView.alpha = 0
        }, completion: { finished in
            self.indicator.removeFromSuperview()
            self.loadingView.removeFromSuperview()
        })
    }
    
    func addLoadingView() {
        loadingView = UIView(frame: CGRect.zero)
        loadingView.backgroundColor = UIColor.white
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        
        
        self.view.addSubview(loadingView)
        
        let leading = NSLayoutConstraint(item: loadingView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: loadingView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
        let top = NSLayoutConstraint(item: loadingView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: loadingView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
        
        self.view.addConstraints([leading, trailing, top, bottom])
        
        
        indicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        indicator.color = UIColor.gray
        indicator.translatesAutoresizingMaskIntoConstraints = false
        self.loadingView.addSubview(indicator)
        
        let centerX = NSLayoutConstraint(item: indicator, attribute: .centerX, relatedBy: .equal, toItem: self.loadingView, attribute: .centerX, multiplier: 1, constant: 0)
        let centerY = NSLayoutConstraint(item: indicator, attribute: .centerY, relatedBy: .equal, toItem: self.loadingView, attribute: .centerY, multiplier: 1, constant: 0)
        
        self.loadingView.addConstraints([centerX, centerY])
    }

}

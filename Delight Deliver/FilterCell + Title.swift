//
//  FilterCell + Title.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 26/07/2016.
//  Copyright © 2016 Artem Ryabukha. All rights reserved.
//

import Foundation


extension FilterCell {
    override func expand() {
        if arrowImageView != nil {
            arrowImageView.image = Images.arrowUp
        }
    }
    
    override func collapse() {
        if arrowImageView != nil {
            arrowImageView.image = Images.arrowDown
        }
    }
}

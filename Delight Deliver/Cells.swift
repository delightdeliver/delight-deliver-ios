//
//  Cells.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 26/07/2016.
//  Copyright © 2016 Artem Ryabukha. All rights reserved.
//

import Foundation
import UIKit

struct Cell {
    
    static let heightes: [String:CGFloat] = [
        // Heightes for FilterCell
        "TopFilterCell": 245.0,
        "TitleFilterCell": 66.0,
        "RegularFilterCell": 54.0,
        "TagsFilterCell": 157.0,
        "SliderFilterCell": 158.0,
        "ButtonFilterCell": 84,
        "RegularSelectionFilterCell": 150,
        "ImageRestaurantPageCell": 257,
        "MainRestaurantPageCell": 175,
        "InfoRestaurantPageCell": 60,
        "MenuRestaurantPageCell": 86,
        "TagsRestaurantPageCell": 160,
        "ButtonRestaurantPageCell": 154,
        "DishOrderCell": 150,
        "TitleEmptyPreorderCell": 80,
        "TitleSelectedPreorderCell": 80,
        "RegularPreorderCell": 59,
        "PeopleNumberPreorderCell": 210,
        "TimePreorderCell": 211,
        "CommentPreorderCell": 110,
        "PersonInfoPreorderCell": 50,
        "DishEmptyPreorderCell": 70,
        "ButtonPreorderCell": 100,
        "TopProfileCell": 160,
        "ProfileCell": 50,
        "ProfileAddressCell": 120,
        "ButtonProfileAddressCell": 180,
        "ProfileEditCell": 60,
        
        "TopOrderHistoryCell": 165,
        "ContentTitleOrderHistoryCell": 65,
        "DishOrderHistoryCell": 120,
        "TitleFromRestaurantPreorderCell": 80
    ]
}

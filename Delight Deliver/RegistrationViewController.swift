//
//  RegistrationViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 20/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailTextField.endEditing(true)
        passwordTextField.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emailTextField.layer.cornerRadius = 5.0
        self.passwordTextField.layer.cornerRadius = 5.0
        
        self.emailTextField.layer.masksToBounds = true
        self.passwordTextField.layer.masksToBounds = true
        
        self.registrationButton.layer.cornerRadius = 5.0
        self.registrationButton.layer.masksToBounds = true
    }
    
    
    @IBAction func dismiss(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func navigateToLogin(_ sender: AnyObject) {
        
    }
    
    @IBAction func register(_ sender: AnyObject) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

//
//  TagsControl.swift
//  Delight Deliver
//
//  Created by Artem on 10/01/16.
//  Copyright © 2016 temi4hik. All rights reserved.
//

import UIKit
import Alamofire


protocol TagsControlDelegate {
    func tagWasToggled(selectedTags: Set<String>)
}

class TagsControl: UIScrollView {
    
    private var tagsButtons = [UIButton]()
    private var tagsDict = [String: UIButton]()
    var tagsArray: [String] = []
    var tagsSet: Set<String> = Set<String>()
    var usedColorsStack = [UIColor]()
    var selectedTags: Set<String> = Set<String>()
    var checkable: Bool = false
    
    var indicator: UIActivityIndicatorView!
    
    var tagsDelegate: TagsControlDelegate!
    
    var colors = [
        UIColor(red: 165/255, green: 217/255, blue: 91/255, alpha: 1),
        UIColor(red: 250/255, green: 236/255, blue: 82/255, alpha: 1),
        UIColor(red: 105/255, green: 218/255, blue: 187/255, alpha: 1),
        UIColor(red: 94/255, green: 218/255, blue: 126/255, alpha: 1),
        UIColor(red: 152/255, green: 232/255, blue: 36/255, alpha: 1),
        UIColor(red: 255/255, green: 177/255, blue: 86/255, alpha: 1),
        UIColor(red: 130/255, green: 146/255, blue: 242/255, alpha: 1),
        UIColor(red: 201/255, green: 137/255, blue: 254/255, alpha: 1),
        UIColor(red: 68/255, green: 204/255, blue: 228/255, alpha: 1),
        UIColor(red: 250/255, green: 117/255, blue: 161/255, alpha: 1),
        UIColor(red: 235/255, green: 201/255, blue: 41/255, alpha: 1)
    ]

    let spacing = 15
    let buttonHeight = 40
    
    
    // MARK: Initialization
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addIndicatorView()
        self.showsHorizontalScrollIndicator = false
    }
    
    func setTags(tags: [String]) {
        self.tagsSet = Set(tags)
        tagsButtons = []
        self.initButtons()
    }
    
    func initButtons() {
        self.isScrollEnabled = true
        var totalWidth: CGFloat = CGFloat(spacing)
        
        for tag in tagsSet {
            let button = UIButton()
            
            
            // Setting button color from avaiable colors list
            button.backgroundColor = getRandomColor()
            button.setTitle("#" + tag, for: .normal)
            button.titleLabel?.font = UIFont(name: "Intro", size: 13)
            
            let font = UIFont.boldSystemFont(ofSize: 13)
            
            button.titleLabel?.font = font
            button.titleLabel?.textColor = UIColor.white
            button.layer.cornerRadius = 20
            tagsButtons += [button]
            
            let colorDelta: CGFloat = 20
            
            
            
            let colors = button.layer.backgroundColor!.components!
            
            button.layer.borderColor = UIColor(red: colors[0] - colorDelta / 255, green: colors[1] - colorDelta / 255, blue: colors[2] - colorDelta / 255, alpha: colors[3]).cgColor
            
            let buttonWidth = (button.titleLabel?.text?.characters.count)! * 10 + 25
            
            
            
            let buttonFrame = CGRect(x: Int(totalWidth), y: 0, width: buttonWidth, height: self.buttonHeight)
            if self.checkable {
                button.addTarget(self, action: #selector(tagsButtonTapped(button:)), for: .touchDown)
            }
            
            button.frame = buttonFrame
            totalWidth += button.frame.width + CGFloat(spacing)
            addSubview(button)
        }
        
        self.contentSize = CGSize(width: totalWidth, height: CGFloat(self.buttonHeight))
    }
    
    override func layoutSubviews() {
        let buttonHeight = self.buttonHeight
        var totalWidth: CGFloat = CGFloat(spacing)
        for (_, button) in tagsButtons.enumerated() {
            let buttonWidth = (button.titleLabel?.text?.characters.count)! * 10 + 25
            
            let buttonFrame = CGRect(x: Int(totalWidth), y: 0, width: buttonWidth, height: buttonHeight)
            button.frame = buttonFrame
            totalWidth += button.frame.width + CGFloat(spacing)
        }
        
        self.contentSize = CGSize(width: totalWidth, height: CGFloat(self.buttonHeight))
//        indicator.center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
    }
    
    
    func getRandomColor() -> UIColor {

        let index = arc4random_uniform(UInt32(colors.count))
        
        return self.colors[Int(index)]
    }
    
    func loadTags() {
        let url = "https://2-dot-delightdelivery2015.appspot.com/api/popular_tags_get/"
        self.tagsSet = Set<String>()
        self.tagsArray = []
        
        self.indicator.startAnimating()
        
        Alamofire.request(url).responseJSON { response in
            if let responseArray = response.result.value as? NSArray {
                let tagsSet = Set(responseArray as! [String])
                self.tagsSet = tagsSet
                self.initButtons()
                self.indicator.stopAnimating()
            } else {
                
            }

        }
    }
    
    func toggleButtonEnabled(button: UIButton) {
        let tagWithSharp = button.titleLabel?.text!
        let index = tagWithSharp?.index(tagWithSharp!.startIndex, offsetBy: 1)
        let tag = tagWithSharp!.substring(from: index!)
        
        
        // Remove from selected
        if self.selectedTags.contains(tag) {
            selectedTags.remove(tag)
            button.layer.borderWidth = 0
        }
        // Add to selected
        else {
            selectedTags.insert(tag)
            button.layer.borderWidth = 2
        }
        
        
//        tagsDelegate.tagWasToggled(selectedTags: selectedTags)
        print(selectedTags)
    }
    
    
    func addIndicatorView() {
        self.indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.indicator.translatesAutoresizingMaskIntoConstraints = false
        self.indicator.hidesWhenStopped = true
        self.addSubview(indicator)
        
        let centerX = NSLayoutConstraint(item: indicator, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0)
        let centerY = NSLayoutConstraint(item: indicator, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0)
        
        self.addConstraints([centerX, centerY])
    }
    
    
    
    func tagsButtonTapped(button: UIButton) {
        toggleButtonEnabled(button: button)
    }
}

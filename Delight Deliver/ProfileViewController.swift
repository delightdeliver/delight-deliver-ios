//
//  ProfileViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 20/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class ProfileViewController: UITableViewController {
    
    var images = [Images.profileOrders, Images.profileStatus, Images.profileAddress, Images.profileEdit, Images.profileExit]
    var labels = ["My orders", "Order status", "Addresses", "Edit profile", "Log out"]
    var identifiers = ["TopProfileCell", "ProfileCell"]
    
    let segues = ["showOrderHistoryFromProfile", "", "showAddressesFromProfile", "showEditForProfile"]
    
    var user: User {
        get {
            return User.user!
        }
    }
    @IBAction func dismiss(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        for id in identifiers {
            tableView.register(UINib(nibName: id, bundle: nil), forCellReuseIdentifier: id)
        }
        tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 4 {
            NetworkHelper.instance.logout(success: { self.dismiss(animated: true, completion: nil) }, failure: nil)
        } else {
            performSegue(withIdentifier: segues[indexPath.row], sender: self)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Cell.heightes[identifiers[indexPath.section]]!
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return 5
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifiers[indexPath.section], for: indexPath) as! ProfileCell
        
        if indexPath.section == 0 {
            cell.emailLabel.text = user.email
            cell.nameLabel.text = user.name!
        } else {
            cell.profileImageView.image = images[indexPath.row]
            cell.profileLabel.text = labels[indexPath.row]
        }
        
        return cell
    }
}

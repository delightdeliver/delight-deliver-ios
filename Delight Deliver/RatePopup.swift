//
//  RatePopup.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 22/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

enum RatePopupType {
    case restaurant, dish
}

class RatePopup: PopupView, RatingControlDelegate {
    @IBOutlet weak var ratingControl: RatingControl! {
        didSet {
            ratingControl.delegate = self
        }
    }
    @IBOutlet weak var ratingLabel: UILabel! {
        didSet {
            ratingLabel.isHidden = true
        }
    }
    @IBOutlet weak var commentTextView: UITextView! {
        didSet {
            commentTextView.delegate = self
            commentTextView.text = commentPlaceholder
            commentTextView.textColor = UIColor.lightGray
            commentTextView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        }
    }
    
    let commentPlaceholder = "Leave a comment"
    var type: RatePopupType = .restaurant
    var id: Int!
    
    var rateTitles = ["Awful", "Bad", "Expected better", "Good!", "Amazing!"]
    
    
    override func makeAction() {
        let comment = commentTextView.text != "" ? commentTextView.text : nil
        let rate = ratingControl.rating
        
        switch self.type {
        case .restaurant:
            NetworkHelper.instance.rateRestaurant(restaurantId: id, rating: rate, comment: comment, success: {
                super.makeAction()
                }, failure: nil)
            break
        default:
            NetworkHelper.instance.rateDish(dishId: id, rating: rate, comment: comment, success: {
                super.makeAction()
                }, failure: nil)
            break
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.commentTextView.endEditing(true)
    }
    
    func didPressRating(rating: Int) {
        ratingLabel.isHidden = false
        self.ratingLabel.text = rateTitles[rating - 1]
    }
    
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let textView = object as! UITextView
        var topCorrect = (textView.bounds.size.height - textView.contentSize.height * textView.zoomScale) / 2
        topCorrect = topCorrect < 0.0 ? 0.0 : topCorrect
        textView.contentInset.top = topCorrect
    }
}

extension RatePopup: UITextViewDelegate {
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            let top = (textView.bounds.size.height - textView.contentSize.height * textView.zoomScale) / 2
            textView.textColor = UIColor.lightGray
            textView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
            textView.text = commentPlaceholder
            textView.contentInset.top = top
            textView.textAlignment = .center
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        // There is placeholder
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.darkGray
            textView.removeObserver(self, forKeyPath: "contentSize")
            textView.contentInset.top = 5.0
            textView.textContainerInset.left = 5.0
            textView.textAlignment = .left
        }
        
        return true
    }
}

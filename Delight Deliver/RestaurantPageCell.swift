//
//  RestaurantPageCell.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 19/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

protocol RestaurantPageCellDelegate {
    func didPressQRCode() -> Void
    func didMakeReservation() -> Void
}

class RestaurantPageCell: UITableViewCell {
    // Interior
    @IBOutlet weak var interiorImageView: UIImageView!
    
    // Main
    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var restaurantMinDeliverySumLabel: UILabel!
    @IBOutlet weak var restaurantCuisinesLabel: UILabel!
    @IBOutlet weak var restaurantDeliveryPriceLabel: UILabel!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var votesLabel: UILabel!
    
    // Info
    @IBOutlet weak var infoImageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    
    // Menu
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    
    
    // Tag
    @IBOutlet weak var foodPrefersLabel: UILabel!
    @IBOutlet weak var tagsControl: TagsControl!
    
    
    // Button
    @IBOutlet weak var reservationButton: UIButton!
    @IBOutlet weak var qrCodeButton: UIButton!
    
    
    
    var delegate: RestaurantPageCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if reservationButton != nil && qrCodeButton != nil {
            reservationButton.layer.masksToBounds = true
            qrCodeButton.layer.masksToBounds = true
            
            reservationButton.layer.cornerRadius = 5.0
            qrCodeButton.layer.cornerRadius = 5.0
            
            qrCodeButton.layer.borderWidth = 1.0
            qrCodeButton.layer.borderColor = UIColor(red: 0.4902, green: 0.8627, blue: 0.2902, alpha: 1).cgColor
        }
    }
    
    
    @IBAction func makeReservation(_ sender: AnyObject) {
        delegate?.didMakeReservation()
    }
    
    @IBAction func getQrCode(_ sender: AnyObject) {
        delegate?.didPressQRCode()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

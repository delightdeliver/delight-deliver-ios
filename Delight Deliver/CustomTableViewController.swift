//
//  CustomTableViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 20/07/2016.
//  Copyright © 2016 Artem Ryabukha. All rights reserved.
//

import UIKit


protocol Expandable {
    func expand()
    func collapse()
}

class CustomTableViewController: UITableViewController {
    
    var cellDescriptors: NSMutableArray!
    var descriptorPath: String!
    var cellNibs: [String]!
    var visibleRowsPerSection: [[Int]]! = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureParams() {
        fatalError("You must implement this function and set here descriptorPath and cellNibs")
    }
    
    func loadTableView() {
        // If function is not implemented – calls an error
        configureParams()
        configureTableView()
        loadCellDescriptors()
    }
    
    private func configureTableView() {
        for nib in self.cellNibs {
            tableView.register(UINib(nibName: nib, bundle: nil), forCellReuseIdentifier: nib)
        }
    }
    
    
    func getIndicesOfVisibleRows() {
        visibleRowsPerSection.removeAll()
        
        
        for currentSectionCells in cellDescriptors.objectEnumerator().allObjects as! [[[String:AnyObject]]] {
            var visibleRows = [Int]()
            
            for row in 0 ... currentSectionCells.count - 1 {
                if (currentSectionCells[row]["isVisible"] as! Bool) {
                    visibleRows.append(row)
                }
            }
            
            self.visibleRowsPerSection.append(visibleRows)
        }
    }
    
    func getCellDescriptorForIndexPath(indexPath: IndexPath) -> [String : AnyObject] {
        let indexOfVisibleRow = visibleRowsPerSection[indexPath.section][indexPath.row]
        
        
        let descriptorsArray = cellDescriptors.object(at: indexPath.section) as! NSArray
        let cellDescriptor = descriptorsArray.object(at: indexOfVisibleRow) as! [String : AnyObject]
        
        return cellDescriptor
    }
    
    
    func loadCellDescriptors() {
        if let path = Bundle.main.path(forResource: descriptorPath, ofType: "plist") {
            cellDescriptors = NSMutableArray(contentsOfFile: path)
            getIndicesOfVisibleRows()
            tableView.reloadData()
        }
    }
    
    func setupCell(cell: UITableViewCell, cellIdentifier: String) {
        // Incomplete implementation
        fatalError("Method must be implemented in inherited class")
    }
}

// MARK: - Table view data source
extension CustomTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard self.cellDescriptors != nil else {
            return 0
        }
        
        return cellDescriptors.count
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard self.visibleRowsPerSection != nil else {
            return 0
        }
        
        return visibleRowsPerSection[section].count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // Check if cellIdentifier key exists
        guard let cellIdentifier = getCellDescriptorForIndexPath(indexPath: indexPath)["cellIdentifier"] as? String else {
            return 0.0
        }
        
        // Check if height for such cellIdentifier exists
        guard let cellHeight = Cell.heightes[cellIdentifier] else {
            return 0.0
        }
        
        return cellHeight
    }
}


// MARK: – Table view delegate
extension CustomTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexOfTapped = visibleRowsPerSection[indexPath.section][indexPath.row]
        var cellDescriptor = ((cellDescriptors.object(at: indexPath.section) as! NSArray).object(at: indexOfTapped)) as! [String:Any]
        
        let cellIdentifier = cellDescriptor["cellIdentifier"] as! String
        
        let collapseEverything = cellDescriptor["collapseEverything"] as? Bool
        
        
        if cellDescriptor["isExpandable"] as! Bool {
            let additionalRows = cellDescriptor["additionalRows"] as! Int
            var shouldExpandAndShowRows = false
            
            if !(cellDescriptor["isExpanded"] as! Bool) {
                shouldExpandAndShowRows = true
            }
            
            
            (((cellDescriptors.object(at: indexPath.section) as! NSArray).object(at: indexOfTapped)) as! NSDictionary).setValue(shouldExpandAndShowRows, forKey: "isExpanded")
            
            var rowsToRemove = 0
            
            if collapseEverything != nil && collapseEverything! && !shouldExpandAndShowRows {
                for j in indexPath.row + 1..<(cellDescriptors.object(at: indexPath.section) as! NSArray).count {
                    
                    (((cellDescriptors.object(at: indexPath.section) as! NSArray).object(at: j)) as! NSDictionary).setValue(false, forKey: "isExpanded")
                    
                    if (((cellDescriptors.object(at: indexPath.section) as! NSArray).object(at: j)) as! NSDictionary).value(forKey: "isVisible") as! Bool {
                        rowsToRemove += 1
                        (((cellDescriptors.object(at: indexPath.section) as! NSArray).object(at: j)) as! NSDictionary).setValue(false, forKey: "isVisible")
                    }
                    
                }
                
                self.getIndicesOfVisibleRows()
                
                var indexPathesToReload: [NSIndexPath] = []
                
                // Getting indexpathes for cells that must be reloaded
                for i in 0..<rowsToRemove {
                    indexPathesToReload.append(NSIndexPath(row: indexPath.row + i + 1, section: indexPath.section))
                }
                
                let cell = tableView.cellForRow(at: indexPath)!
                
                if shouldExpandAndShowRows {
                    tableView.insertRows(at: indexPathesToReload as [IndexPath], with: .fade)
                    cell.expand()
                } else {
                    tableView.deleteRows(at: indexPathesToReload as [IndexPath], with: .fade)
                    cell.collapse()
                }
                
                tableView.deselectRow(at: indexPath, animated: true)
                
                return
            }
            
            
            var i = indexOfTapped + 1
            var enabledCells = 0
            
            while enabledCells < additionalRows {
                let parentId = (((cellDescriptors.object(at: indexPath.section) as! NSArray).object(at: i)) as! NSDictionary).value(forKey: "parent") as! String
                
                if parentId == cellIdentifier {
                    (((cellDescriptors.object(at: indexPath.section) as! NSArray).object(at: i)) as! NSDictionary).setValue(shouldExpandAndShowRows, forKey: "isVisible")
                    enabledCells += 1
                }
                
                i += 1
            }
            
            self.getIndicesOfVisibleRows()
            
            var indexPathesToReload: [NSIndexPath] = []
            
            // Getting indexpathes for cells that must be reloaded
            for i in 0..<additionalRows {
                indexPathesToReload.append(NSIndexPath(row: indexPath.row + i + 1, section: indexPath.section))
            }
            
            let cell = tableView.cellForRow(at: indexPath)!
            
            if shouldExpandAndShowRows {
                tableView.insertRows(at: indexPathesToReload as [IndexPath], with: .fade)
                cell.expand()
            } else {
                tableView.deleteRows(at: indexPathesToReload as [IndexPath], with: .fade)
                cell.collapse()
            }
            
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}

extension UITableViewCell: Expandable {
    func expand() {
        // Empty implementation, should be implemented in derived class
        fatalError("Method must be implemented in inherited class")
    }
    func collapse() {
        // Empty implementation, should be implemented in derived class
        fatalError("Method must be implemented in inherited class")
    }
    
}












//
//  Basket.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 07/03/16.
//  Copyright © 2016 temi4hik. All rights reserved.
//

import Foundation
import UIKit

class Basket {
    
    var selectedDishesOrder: [Dish : Int] = [:]
    var selectedDishesPreorder: [Dish : Int] = [:]
    
    var orderRestaurant: Restaurant!
    var preorderRestaurant: Restaurant!
    
    class var instance: Basket {
        struct Singleton {
            static let instance = Basket()
        }
        return Singleton.instance
    }
    
    private init() {
        
    }
    
}








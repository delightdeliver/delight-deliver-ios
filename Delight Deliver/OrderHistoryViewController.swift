//
//  OrderHistoryViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 20/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class OrderHistoryViewController: CustomTableViewController {
    
    var orders: [Order] = []
    var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        indicator = UIActivityIndicatorView()
        indicator.activityIndicatorViewStyle = .whiteLarge
        indicator.color = UIColor.gray
        indicator.hidesWhenStopped = true
        
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)

        super.viewDidLoad()
    }
    
    func refresh(sender: Any) {
        self.loadCellDescriptors()
    }
    
    override func configureParams() {
        cellNibs = ["TopOrderHistoryCell", "ContentTitleOrderHistoryCell", "DishOrderHistoryCell"]
    }
    
    override func loadCellDescriptors() {
        
        self.tableView.backgroundView = indicator
        indicator.startAnimating()
        
        NetworkHelper.instance.loadOrderHistory(success: { orders in
            self.orders = orders
            self.setupCellDescriptors()
            self.getIndicesOfVisibleRows()
            self.indicator.stopAnimating()
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
            
            }, failure: nil)
    }
    
    func setupCellDescriptors() {
        
        self.cellDescriptors = NSMutableArray()
        
        for order in orders {
            let sectionArray = NSMutableArray()
            
            // First element – TopOrderHistoryCell
            let topDict = NSMutableDictionary()
            topDict.setValue(order.restaurant.name, forKey: "restName")
            topDict.setValue(order.restaurant.picture, forKey: "restPicture")
            topDict.setValue(order.status, forKey: "orderStatus")
            topDict.setValue(order.acceptedTime, forKey: "accepted_time")
            
            topDict.setValue("TopOrderHistoryCell", forKey: "cellIdentifier")
            topDict.setValue(true, forKey: "isVisible")
            topDict.setValue(false, forKey: "isExpanded")
            topDict.setValue(false, forKey: "isExpandable")
            topDict.setValue(0, forKey: "additionalRows")
            
            sectionArray.add(topDict)
            
            // Second element - ContentTitleOrderHistoryCell
            let contentTopDict = NSMutableDictionary()
            contentTopDict.setValue(order.dishes.count, forKey: "dishAmount")
            
            contentTopDict.setValue("ContentTitleOrderHistoryCell", forKey: "cellIdentifier")
            contentTopDict.setValue(true, forKey: "isVisible")
            contentTopDict.setValue(true, forKey: "isExpandable")
            contentTopDict.setValue(false, forKey: "isExpanded")
            contentTopDict.setValue(order.dishes.count, forKey: "additionalRows")
            sectionArray.add(contentTopDict)
            
            // Any other elements - DishHistoryOrderCell
            for (dish, _) in order.dishes {
                let dishDict = NSMutableDictionary()
                dishDict.setValue("DishOrderHistoryCell", forKey: "cellIdentifier")
                dishDict.setValue(false, forKey: "isVisible")
                dishDict.setValue(false, forKey: "isExpanded")
                dishDict.setValue(false, forKey: "isExpandable")
                dishDict.setValue("ContentTitleOrderHistoryCell", forKey: "parent")
                dishDict.setValue(0, forKey: "additionalRows")
                dishDict.setValue(dish, forKey: "dish")
                
                sectionArray.add(dishDict)
            }
            
            cellDescriptors.add(sectionArray)
        }
    }
}

// TableView datasource
extension OrderHistoryViewController {
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 3.0
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellDescriptor = getCellDescriptorForIndexPath(indexPath: indexPath)
        let cellIdentifier = cellDescriptor["cellIdentifier"] as! String
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! OrderHistoryCell
        let order = self.orders[indexPath.section]
        
        switch cellIdentifier {
        case "TopOrderHistoryCell":
            
            cell.selectionStyle = .none
            if let pic = order.restaurant.picture {
                if let url = URL(string: pic) {
                    cell.restautantImageView.af_setImage(withURL: url)
                }
            }
            
            cell.restaurantNameLabel.text = order.restaurant.name.capitalizingFirstLetter()
            cell.orderSumLabel.text = "\(order.orderSum) R"
            cell.orderStatusLabel.text = order.status.title
            cell.orderTimeLabel.text = order.acceptedTime
            break
        case "ContentTitleOrderHistoryCell":
            let expanded = cellDescriptor["isExpanded"] as! Bool
            
            if expanded {
                cell.expand()
            } else {
                cell.collapse()
            }
            break
        case "DishOrderHistoryCell":
            let dish = cellDescriptor["dish"] as! Dish
            cell.selectionStyle = .none
            if let pic = dish.picture {
                if let url = URL(string: pic) {
                    cell.dishImageView.af_setImage(withURL: url, completion: { data in
                        cell.dishImageView.makeCircle()
                    })
                    
                }
            }
            
            cell.dishNameLabel.text = dish.dishName.capitalizingFirstLetter()
            
            if let descr = dish.dishDescription {
                cell.dishDescriptionLabel.text = descr.capitalizingFirstLetter()
            }
            
            let amount = order.dishes[dish]!
            cell.dishCountLabel.text = "\(amount)"
            cell.dishPriceLabel.text = "\(dish.price!) R"
            
            break
        default:
            break
        }
        
        return cell
    }
}
// TableView delegate
extension OrderHistoryViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
//            self.tableView.deselectRow(at: indexPath, animated: false)
            
            let controller = PopupController(type: .rateRestaurant)
            let restaurant = self.orders[indexPath.section].restaurant
            (controller.popupView as! RatePopup).id = restaurant.id
            
            controller.popupView.titleString = "Have you enjoyed order?"
            
            controller.showPopup(viewController: self)
        } else if indexPath.row > 1 {
//            self.tableView.deselectRow(at: indexPath, animated: true)
            
            let descriptor = getCellDescriptorForIndexPath(indexPath: indexPath)
            let dish = descriptor["dish"] as! Dish
            let controller = PopupController(type: .rateDish)
            (controller.popupView as! RatePopup).id = dish.id
            
            controller.popupView.titleString = "Have you enjoyed dish?"
            
            controller.showPopup(viewController: self)
        } else {
            super.tableView(tableView, didSelectRowAt: indexPath)
        }
    }
}












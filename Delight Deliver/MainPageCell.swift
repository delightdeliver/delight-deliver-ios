//
//  MainPageCell.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 16/07/2016.
//  Copyright © 2016 Artem Ryabukha. All rights reserved.
//

import UIKit

class MainPageCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
}

//
//  FilterCell + Regular.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 26/07/2016.
//  Copyright © 2016 Artem Ryabukha. All rights reserved.
//

import Foundation

enum TextCellType: String {
    case Cuisine = "cuisine"
    case DishName = "category"
    case Ingredients = "ingredients"
}


extension FilterCell {
    
}

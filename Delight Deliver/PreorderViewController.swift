//
//  PreorderViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 19/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class PreorderViewController: CustomTableViewController {
    
    var restaurant: Restaurant?
    var selectedDishes: [Dish:Int] = [:]
    var selectedTimeStr: String?
    var selectedTime: Date?
    var peopleNum: Int?
    var comment = ""
    var headerHeightes: [CGFloat] = [0.1, 50, 42, 15, 0.1]
    var headerTitles: [String?] = [nil, "Booking information", "Personal information", nil, nil]
    
    var personInfo: [String:String]?
    
    var fromRestaurant: Bool = false
    
    var totalPrice: Int {
        get {
            var sum = 0
            for (dish, amount) in self.selectedDishes {
                sum += dish.price * amount
            }
            return sum
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        if (fromRestaurant) {
            ((cellDescriptors[0] as! NSMutableArray)[0] as! NSMutableDictionary).setValue(false, forKey: "isVisible")
            ((cellDescriptors[0] as! NSMutableArray)[2] as! NSMutableDictionary).setValue(true, forKey: "isVisible")
            
            getIndicesOfVisibleRows()
            tableView.reloadSections(IndexSet(integer: 0), with: .fade)
        }
        
    }
    
    override func configureParams() {
        
        tableView.backgroundView?.backgroundColor = UIColor(red: 0.9686, green: 0.9725, blue: 0.9765, alpha: 1.0)
        tableView.backgroundColor = UIColor(red: 0.9686, green: 0.9725, blue: 0.9765, alpha: 1.0)
        
        self.descriptorPath = "PreorderDescriptor"
        
        cellNibs = ["TitleEmptyPreorderCell", "TitleSelectedPreorderCell", "TitleFromRestaurantPreorderCell", "RegularPreorderCell", "PeopleNumberPreorderCell", "TimePreorderCell", "CommentPreorderCell", "PersonInfoPreorderCell", "ButtonPreorderCell", "DishOrderCell", "DishEmptyPreorderCell"]
    }
    
    
    func updateDishCellDescriptors(shouldRemoveCells: Bool) -> Void {
        let preorderDishesSection = 3
        let preorderEmptyDishRow = 1
        let dishId = "DishOrderCell"
        
        let hasSelectedDishes = self.selectedDishes.count != 0
        
        ((cellDescriptors[preorderDishesSection] as! NSMutableArray)[preorderEmptyDishRow] as! NSMutableDictionary).setValue(!hasSelectedDishes, forKey: "isVisible")
        
        let unusedDishesCount = (cellDescriptors[preorderDishesSection] as! NSArray).count - 2
        
        (cellDescriptors[preorderDishesSection] as! NSMutableArray).removeObjects(in: NSRange(location: 2, length: unusedDishesCount))
        
        if hasSelectedDishes {
            for (dish, amount) in self.selectedDishes {
                let dishDict: NSMutableDictionary = NSMutableDictionary()
                
                dishDict.setValue(0, forKey: "additionalRows")
                dishDict.setValue(false, forKey: "isExpandable")
                dishDict.setValue(false, forKey: "isExpandable")
                dishDict.setValue(dishId, forKey: "cellIdentifier")
                dishDict.setValue(amount, forKey: "amount")
                dishDict.setValue(dish.dishName, forKey: "dishName")
                dishDict.setValue(dish.price, forKey: "dishPrice")
                
                if let desc = dish.dishDescription {
                    dishDict.setValue(desc, forKey: "dishDescription")
                } else {
                    dishDict.setValue("", forKey: "dishDescription")
                }
                
                if let dishPic = dish.picture {
                    dishDict.setValue(dishPic, forKey: "dishPicture")
                }
                
                if let ingridients = dish.ingridients {
                    dishDict.setValue(ingridients.joined(separator: ", ").capitalizingFirstLetter(), forKey: "ingridients")
                }
                
                
                (cellDescriptors[preorderDishesSection] as! NSMutableArray).add(dishDict)
                
            }
        }
        
        getIndicesOfVisibleRows()
        if shouldRemoveCells {
            tableView.reloadSections(IndexSet(integer: preorderDishesSection), with: .fade)
        } else {
            tableView.reloadData()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRestaurantsFromPreorder" {
            let destination = segue.destination as! RestaurantListViewController
            destination.hideFooter = true
            destination.delegate = self
        } else if segue.identifier == "showRestaurantMenuFromPreorder" {
            let destination = segue.destination as! RestaurantMenuViewController
            destination.restaurant = self.restaurant!
            destination.categories = self.restaurant!.categories
            destination.currentTabIndex = 0
        }
    }
    
    
}

extension PreorderViewController: RestaurantListDelegate {
    func setRest(restaurant: Restaurant) {
        self.restaurant = restaurant
        
        let cellSection = 0
        let cellRow = 0
        
        ((cellDescriptors[cellSection] as! NSMutableArray)[cellRow] as! NSMutableDictionary).setValue(false, forKey: "isVisible")
        ((cellDescriptors[cellSection] as! NSMutableArray)[cellRow + 1] as! NSMutableDictionary).setValue(true, forKey: "isVisible")
        
        getIndicesOfVisibleRows()
        tableView.reloadSections(IndexSet(integer: cellSection), with: .fade)
    }
}

// TableView Datasource
extension PreorderViewController {
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return headerTitles[section]
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeightes[section]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentCellDescriptor = getCellDescriptorForIndexPath(indexPath: indexPath)
        let cellIdentifier = currentCellDescriptor["cellIdentifier"] as! String
        
        let dishCell: UITableViewCell
        
        if cellIdentifier == "DishOrderCell" {
            dishCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DishOrderCell
            // Setup dish cell
            return dishCell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PreorderCell
        
        cell.delegate = self
        cell.selectionStyle = .none
        
        switch cellIdentifier {
        case "TitleEmptyPreorderCell":
            // we can do nothing
            cell.selectionStyle = .default
            break
        case "TitleSelectedPreorderCell":
            // setup restaurant cell
            if let rest = self.restaurant {
                if let pic = rest.picture {
                    if let url = URL(string: pic) {
                        cell.restaurantImageView.af_setImage(withURL: url)
                    }
                }
                cell.restaurantNameLabel.text = rest.name.capitalizingFirstLetter()
                cell.restaurantAddressLabel.text = rest.addresses[0]
            }
            break
        case "TitleFromRestaurantPreorderCell":
            cell.restaurantNameLabel.text = self.restaurant!.name.capitalizingFirstLetter()
            break
        case "RegularPreorderCell":
            let typeStr = currentCellDescriptor["type"] as! String
            let type = PreorderCellType(rawValue: typeStr)!
            
            
            if type == .DateAndTime {
                cell.selectionStyle = .default
                if let time = self.selectedTimeStr {
                    cell.regularTitleLabel.text = time
                } else {
                    cell.regularTitleLabel.text = type.title
                }
            } else if type == .NumberOfPerson {
                cell.selectionStyle = .default
                if let num = self.peopleNum {
                    cell.regularTitleLabel.text = "\(num) persons"
                } else {
                    cell.regularTitleLabel.text = type.title
                }
            } else {
                cell.regularTitleLabel.text = type.title
            }
            break
        case "PeopleNumberPreorderCell":
            
            if let num = self.peopleNum {
                cell.peoplePickerView.selectRow(num - 1, inComponent: 0, animated: false)
            }
            break
        case "TimePreorderCell":
            if let time = self.selectedTime {
                cell.datePicker.setDate(time, animated: false)
            }
            break
        case "CommentPreorderCell":
            
            if self.comment.characters.count > 0 {
                cell.commentTextView.text = self.comment
            } else {
                cell.commentTextView.text = PreorderCellType(rawValue: "comment")!.title
                cell.commentTextView.textColor = UIColor.lightGray
            }
            
            break
        case "PersonInfoPreorderCell":
            
            let typeStr = currentCellDescriptor["type"] as! String
            let type = PreorderCellType(rawValue: typeStr)!
            
            cell.cellType = type
            
            if let val = self.personInfo?[typeStr] {
                cell.personInfoTextField.text = val
            }
            cell.personInfoTextField.placeholder = type.title
            cell.personInfoTitleLabel.text = type.title
            break
        default:
            break
        }
        
        return cell
    }
}


extension PreorderViewController: PreorderCellDelegate {
    func didSelectNumberOfPeople(number: Int) {
        let cell = tableView.cellForRow(at: IndexPath(row: 1, section: 1)) as! PreorderCell
        if number == 1 {
            cell.regularTitleLabel.text = "\(number) person"
        } else {
            cell.regularTitleLabel.text = "\(number) persons"
        }
        
    }
    
    func didSetDateAndTime(newDate: Date, newDateStr: String, newTimeStr: String) {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as! PreorderCell
        
        cell.regularTitleLabel.text = "\(newDateStr) \(newTimeStr)"
    }
    func didSetPersonValue(key: String, value: String) {
        
    }
    func didSetComment(comment: String) {
        
    }
    func didPlaceOrder() {
        
    }
    func didRemoveRestaurant() {
        self.restaurant = nil
        
        let cellSection = 0
        let cellRow = 0
        
        ((cellDescriptors[cellSection] as! NSMutableArray)[cellRow] as! NSMutableDictionary).setValue(true, forKey: "isVisible")
        ((cellDescriptors[cellSection] as! NSMutableArray)[cellRow + 1] as! NSMutableDictionary).setValue(false, forKey: "isVisible")
        
        getIndicesOfVisibleRows()
        tableView.reloadSections(IndexSet(integer: cellSection), with: .fade)
    }
}

// TableView Delegate
extension PreorderViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellDescriptor = getCellDescriptorForIndexPath(indexPath: indexPath)
        let cellId = cellDescriptor["cellIdentifier"] as! String
        
        
        switch cellId {
        case "TitleEmptyPreorderCell":
            self.tableView.deselectRow(at: indexPath, animated: true)
            self.performSegue(withIdentifier: "showRestaurantsFromPreorder", sender: self)
            break
        case "DishEmptyPreorderCell":
            if self.restaurant != nil {
                self.restaurant!.loadMenu {
                    for (key, _) in self.restaurant!.menu {
                        self.restaurant!.categories.append(key)
                    }
                    
                    self.performSegue(withIdentifier: "showRestaurantMenuFromPreorder", sender: self)
                }
            } else {
                // Call error alert
            }
            
            break
        default:
            super.tableView(tableView, didSelectRowAt: indexPath)
        }
    }
}








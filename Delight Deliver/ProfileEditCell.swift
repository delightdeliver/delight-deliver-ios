//
//  ProfileEditCell.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 20/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit


enum ProfileEditCellType: String {
    case name = "name"
    case surname = "surname"
    case email = "email"
    case phone = "phone"
    case passwordOld = "password_old"
    case passwordNew = "password_new"
    case passwordRepeat = "password_repeat"
    
    var title: String {
        return LocalizableStrings.getString(key: "profile_edit_\(self.rawValue)_title")
    }
}

class ProfileEditCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var type: ProfileEditCellType! {
        didSet {
            self.titleLabel.text = type.title
            
            switch self.type! {
            case .name:
                self.textField.text = User.user?.name
            case .surname:
                self.textField.text = User.user?.surname
            case .email:
                self.textField.text = User.user?.email
            case .phone:
                self.textField.text = User.user?.phone
            default:
                self.textField.isSecureTextEntry = true
                self.textField.text = nil
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  ProfileEditViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 20/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class ProfileEditViewController: UITableViewController {
    
    var types: [ProfileEditCellType] = [.name, .surname, .email, .phone, .passwordOld, .passwordNew, .passwordRepeat]
    
    var user: User {
        get {
            return User.user!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.register(UINib(nibName: "ProfileEditCell", bundle: nil), forCellReuseIdentifier: "ProfileEditCell")
        
        self.tableView.reloadData()
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Cell.heightes["ProfileEditCell"]!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveInfo(_ sender: AnyObject) {
        let nameCell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfileEditCell
        let surnameCell = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! ProfileEditCell
        let emailCell = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! ProfileEditCell
        let phoneCell = tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! ProfileEditCell
        
        let name = nameCell.textField.text != nil ? nameCell.textField.text! : ""
        let surname = surnameCell.textField.text != nil ? surnameCell.textField.text! : ""
        let email = emailCell.textField.text != nil ? emailCell.textField.text! : ""
        let phone = phoneCell.textField.text != nil ? phoneCell.textField.text! : ""
        
        
        NetworkHelper.instance.setUserInfo(name: name, surname: surname, email: email, phone: phone, addresses: nil, success: {
            
            self.user.name = name
            self.user.surname = surname
            self.user.email = email
            self.user.phone = phone
            
            print("Good")
            
        }, failure: { error in
            
            print("Error")
        })
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Personal data"
        } else {
            return "Password"
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 4
        } else {
            return 3
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileEditCell", for: indexPath) as! ProfileEditCell
        
        cell.type = self.types[indexPath.row + indexPath.section * 4]
        
        return cell
    }
}

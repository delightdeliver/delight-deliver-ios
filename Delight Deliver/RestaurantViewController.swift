//
//  RestaurantViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 18/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class RestaurantViewController: UITableViewController {
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    
    var restaurant: Restaurant!
    var menuCategories: [String] = []
    var selectedCategoryIndex: Int!
    
    var reuseIdentifiers = [["ImageRestaurantPageCell", "MainRestaurantPageCell", "InfoRestaurantPageCell"], [], ["TagsRestaurantPageCell"], ["ButtonRestaurantPageCell"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.title = self.restaurant.name.capitalizingFirstLetter()
        self.setupTableView()
        
        for _ in restaurant.addresses {
            reuseIdentifiers[0].append("InfoRestaurantPageCell")
        }
        
        for (key, _) in self.restaurant.menu {
            self.reuseIdentifiers[1].append("MenuRestaurantPageCell")
            self.menuCategories.append(key)
        }
        
        self.tableView.reloadData()
    }
    
    
    
    
    func setupTableView() {
        tableView.register(UINib(nibName: "InfoRestaurantPageCell", bundle: nil), forCellReuseIdentifier: "InfoRestaurantPageCell")
        tableView.register(UINib(nibName: "MainRestaurantPageCell", bundle: nil), forCellReuseIdentifier: "MainRestaurantPageCell")
        tableView.register(UINib(nibName: "ImageRestaurantPageCell", bundle: nil), forCellReuseIdentifier: "ImageRestaurantPageCell")
        tableView.register(UINib(nibName: "MenuRestaurantPageCell", bundle: nil), forCellReuseIdentifier: "MenuRestaurantPageCell")
        tableView.register(UINib(nibName: "TagsRestaurantPageCell", bundle: nil), forCellReuseIdentifier: "TagsRestaurantPageCell")
        tableView.register(UINib(nibName: "ButtonRestaurantPageCell", bundle: nil), forCellReuseIdentifier: "ButtonRestaurantPageCell")
        
        
        tableView.backgroundView?.backgroundColor = UIColor.white
        tableView.backgroundColor = UIColor.white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMenuFromRestaurant" {
            let destination = segue.destination as! RestaurantMenuViewController
            destination.restaurant = self.restaurant
            destination.categories = self.menuCategories
            destination.currentTabIndex = self.selectedCategoryIndex
        } else if segue.identifier == "showPreorderFromRestaurant" {
            let destination = segue.destination as! PreorderViewController
            destination.fromRestaurant = true
            destination.restaurant = self.restaurant
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            return headerView
        } else {
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return headerView.frame.height
        } else {
            return 0.1
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //showMenuFromRestaurant
        let reuseIdentifier = reuseIdentifiers[indexPath.section][indexPath.row]
        
        if reuseIdentifier == "MenuRestaurantPageCell" {
            self.selectedCategoryIndex = indexPath.row
            self.performSegue(withIdentifier: "showMenuFromRestaurant", sender: self)
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    // Datasource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return reuseIdentifiers.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reuseIdentifiers[section].count
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Cell.heightes[reuseIdentifiers[indexPath.section][indexPath.row]]!
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseId = reuseIdentifiers[indexPath.section][indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId, for: indexPath) as! RestaurantPageCell
        
        cell.delegate = self
        cell.selectionStyle = .none
        
        switch reuseId {
        case "ImageRestaurantPageCell":
            if let urlStr = restaurant.pictureInterior {
                cell.interiorImageView.af_setImage(withURL: URL(string: urlStr)!)
            }
            break
        case "MainRestaurantPageCell":
            
            cell.restaurantNameLabel.text = restaurant.name.capitalizingFirstLetter()
            cell.ratingControl.rating = restaurant.rating
            cell.votesLabel.text = ""
            cell.restaurantCuisinesLabel.text = restaurant.cuisines.joined(separator: ", ")
            
            if restaurant.deliveryCost != nil {
                cell.restaurantDeliveryPriceLabel.text = "\(restaurant.deliveryCost!)"
            } else {
                cell.restaurantDeliveryPriceLabel.text = "Free"
            }
            
            if restaurant.minimumSum != nil {
                cell.restaurantMinDeliverySumLabel.text = "\(restaurant.minimumSum!) R"
            }
            
            
            if let pic = restaurant.picture {
                if let url = NSURL(string: pic) {
                    cell.restaurantImageView.af_setImage(withURL: url as URL)
                }
            }
            break
        case "InfoRestaurantPageCell":
            if indexPath.row == reuseIdentifiers[0].count - 1 {
                cell.infoImageView.image = Images.workTime
                cell.infoLabel.text = "From \(restaurant.workFrom!) to \(restaurant.workTo!)"
            } else {
                cell.infoImageView.image = Images.location
                cell.infoLabel.text = restaurant.addresses[indexPath.row - 2]
            }
            break
        case "MenuRestaurantPageCell":
            var category = menuCategories[indexPath.row]
            category.capitalizeFirstLetter()
            cell.categoryLabel.text = category
            cell.selectionStyle = .default
            break
        case "TagsRestaurantPageCell":
            cell.tagsControl.setTags(tags: restaurant.tags)
            cell.tagsControl.checkable = false
            break
        default:
            break
        }
        
        return cell
    }
}
extension RestaurantViewController: RestaurantPageCellDelegate {
    func didPressQRCode() {
        
    }
    func didMakeReservation() {
        self.performSegue(withIdentifier: "showPreorderFromRestaurant", sender: self)
    }
}











//
//  SearchViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 24/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class SearchViewController: UITableViewController {
    
    @IBOutlet var headerView: UIView!
    @IBOutlet var segmentedControl: UISegmentedControl!
    
    var selectedDish: Dish!
    var selectedDishIdx: Int!
    var selectedRest: Restaurant!
    
    var filteredDishes: [Dish] = []
    var filteredRestaurants: [Restaurant] = []
    
    var dishFilterStr: String = ""
    var restFilterStr: String = ""
    
    var restaurants: [Restaurant] {
        get {
            return RestaurantHelper.instance.restaurants
        }
    }
    var dishes: [Dish] {
        get {
            return DishHelper.instance.dishes
        }
    }
    
    var segment: Int {
        get {
            return self.segmentedControl.selectedSegmentIndex
        }
    }
    
    
    var reuseIdentifier: String {
        get{
            return segment == 0 ? "DishSearchCell" : "RestaurantSearchCell"
        }
    }
    
    var filteredValues: [NSRange] = []
    var searchController: UISearchController!
    
    
    let col = UIColor(red: 0.4902, green: 0.8627, blue: 0.2902, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableHeaderView = headerView
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = false
        self.navigationItem.titleView = self.searchController.searchBar
        
        self.searchController.searchResultsUpdater = self
        tableView.register(UINib(nibName: "DishSearchCell", bundle: nil), forCellReuseIdentifier: "DishSearchCell")
        tableView.register(UINib(nibName: "RestaurantSearchCell", bundle: nil), forCellReuseIdentifier: "RestaurantSearchCell")
        
        
        tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return segment == 0 ? filteredDishes.count : filteredRestaurants.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return segment == 0 ? 50 : 120
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! SearchCell
        
        
        if segment == 0 {
            cell.titleLabel.text = filteredDishes[indexPath.row].dishName.capitalizingFirstLetter()
            cell.titleLabel.setTextColor(color: col, range: filteredValues[indexPath.row])
        } else {
            
            let rest = filteredRestaurants[indexPath.row]
            cell.titleLabel.text = rest.name.capitalizingFirstLetter()
            cell.titleLabel.setTextColor(color: col, range: filteredValues[indexPath.row])
            
            cell.restaurantImageView.af_setImage(withURL: URL(string: rest.picture!)!)
            cell.restaurantCuisineLabel.text = rest.cuisines.joined(separator: ", ")
            
            cell.ratingControl.rating = rest.rating
            
            if let ratNum = rest.numberOfVotes {
                cell.ratingLabel.text = "(\(ratNum))"
            } else {
                cell.ratingLabel.text = ""
            }
        }
        
        return cell
    }
    
    @IBAction func segmentChanged(_ sender: AnyObject) {
        self.searchController.searchBar.text = segment == 0 ? dishFilterStr : restFilterStr
        self.tableView.reloadData()
    }
    
    func filterRestaurants() {
        filteredValues = []
        filteredRestaurants = []
        
        guard restFilterStr != "" else {
            self.tableView.reloadData()
            return
        }
        
        let filterStr = restFilterStr.lowercased().replacingOccurrences(of: "ё", with: "е")
        
        
        outerloop:
            for rest in self.restaurants {
                let restWords = rest.name.lowercased().components(separatedBy: " ")
                let restStr = rest.name.lowercased().replacingOccurrences(of: "ё", with: "е") as NSString
                
                if restStr.hasPrefix(filterStr) {
                    filteredValues.append(restStr.range(of: filterStr))
                    filteredRestaurants.append(rest)
                    continue outerloop
                }
                
                for word in restWords {
                    if word.hasPrefix(filterStr) {
                        filteredValues.append(restStr.range(of: filterStr))
                        filteredRestaurants.append(rest)
                        continue outerloop
                    }
                }
        }
        self.tableView.reloadData()
    }
    
    func filterDishes() {
        
        filteredValues = []
        filteredDishes = []
        
        guard dishFilterStr != "" else {
            self.tableView.reloadData()
            return
        }
        
        let filterStr = dishFilterStr.lowercased().replacingOccurrences(of: "ё", with: "е")
        
        outerLoop:
        for dish in self.dishes {
            let dishWords = dish.dishName.lowercased().components(separatedBy: " ")
            let dishStr = dish.dishName.lowercased().replacingOccurrences(of: "ё", with: "е") as NSString
            
            if dishStr.hasPrefix(filterStr) {
                filteredValues.append(dishStr.range(of: filterStr))
                filteredDishes.append(dish)
                continue
            }
            
            for word in dishWords {
                if word.hasPrefix(filterStr) {
                    filteredValues.append(dishStr.range(of: filterStr))
                    filteredDishes.append(dish)
                    continue outerLoop
                }
            }
        }
        
        self.tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDishFromSearch" {
            let controller = segue.destination as! DishViewController
            controller.dishes = filteredDishes
            controller.selectedDishIndex = selectedDishIdx
            controller.restaurantHidden = false
        } else if segue.identifier == "showRestaurantFromSearch" {
            let controller = segue.destination as! RestaurantViewController
            controller.restaurant = selectedRest
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if segmentedControl.selectedSegmentIndex == 0 {
            self.selectedDishIdx = indexPath.row
            self.performSegue(withIdentifier: "showDishFromSearch", sender: self)
            self.tableView.deselectRow(at: indexPath, animated: true)
        } else {
            selectedRest = filteredRestaurants[indexPath.row]
            self.performSegue(withIdentifier: "showRestaurantFromSearch", sender: self)
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
        
    }
}

extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if segmentedControl.selectedSegmentIndex == 0 {
            dishFilterStr = searchController.searchBar.text!
            filterDishes()
        } else {
            restFilterStr = searchController.searchBar.text!
            filterRestaurants()
        }
        
    }
}

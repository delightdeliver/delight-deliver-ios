//
//  RatingControl.swift
//  Delight Deliver
//
//  Created by Artem on 25/12/15.
//  Copyright © 2015 temi4hik. All rights reserved.
//

import UIKit

protocol RatingControlDelegate {
    func didPressRating(rating: Int) -> Void
}

class RatingControl: UIView {
    // MARK: Properties
    
    var rating = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    var ratingButtons = [UIButton]()
    
    var stars = 5
    
    var spacing: CGFloat {
        get {
            return (self.frame.width - self.buttonSize * CGFloat(stars)) / CGFloat(stars - 1)
        }
    }
    
    var buttonSize: CGFloat {
        get {
            return self.frame.height
        }
    }
    
    var delegate: RatingControlDelegate?
    
    // MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initButtons()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initButtons()
    }
    
    func initButtons() {
        let filledStarImage = UIImage(named: "StarFilled")
        let emptyStarImage = UIImage(named: "StarEmpty")
        
        for _ in 0..<self.stars {
            let button = UIButton()
            
            button.setImage(emptyStarImage, for: .normal)
            button.setImage(filledStarImage, for: .selected)
            button.setImage(filledStarImage, for: [.highlighted, .selected])
            button.adjustsImageWhenHighlighted = false
            button.addTarget(self, action: #selector(ratingButtonTapped(button:)), for: .touchDown)
            ratingButtons += [button]
            addSubview(button)
        }
    }
    
    override func layoutSubviews() {
        // Set the button's width and height to a square the size of the frame's height.
        var buttonFrame = CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize)
        
        // Offset each button's origin by the length of the button plus spacing.
        for (index, button) in ratingButtons.enumerated() {
            buttonFrame.origin.x = CGFloat(index) * (buttonSize + spacing)
            button.frame = buttonFrame
        }
        updateButtonSelectionStates()
    }
    
    override public var intrinsicContentSize: CGSize {
        get {
            let width = (buttonSize + spacing) * CGFloat(stars)
            
            return CGSize(width: width, height: buttonSize)
        }
    }
    
    // MARK: Button Action
    
    func ratingButtonTapped(button: UIButton) {
        rating = ratingButtons.index(of: button)! + 1
        updateButtonSelectionStates()
        delegate?.didPressRating(rating: rating)
    }
    
    func updateButtonSelectionStates() {
        for (index, button) in ratingButtons.enumerated() {
            // If the index of a button is less than the rating, that button should be selected.
            button.isSelected = index < rating
        }
    }
}

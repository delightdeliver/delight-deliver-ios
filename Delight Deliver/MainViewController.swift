//
//  MainViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 16/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

private let reuseIdentifier = "MainPageCell"

class MainViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var categories = [[String:String]]()
    
    var cellWidth: CGFloat {
        get {
            return (ScreenSize.minScreenLength - 2) / 2
        }
    }
    
    
    
    var cellHeight: CGFloat {
        get {
            if DeviceType.iPad {
                return cellWidth * Ratio.mainPageIpadRatio
            } else {
                return cellWidth * Ratio.mainPageIphoneRatio
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.collectionView.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        self.setupCollectionView()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setupCollectionView() {
        
        let resourceArray: NSArray?
        if let path = Bundle.main.path(forResource: "MainPageItems", ofType: "plist") {
            resourceArray = NSArray(contentsOfFile: path)
            
            if resourceArray != nil {
                self.categories = resourceArray as! [[String:String]]
                self.collectionView?.reloadData()
            }
        }
    }
    
    
    @IBAction func showUser(_ sender: AnyObject) {
        
        let userStoryboard = UIStoryboard(name: "Auth", bundle: nil)
        
        let controller: UIViewController
        
        if User.user != nil {
            controller = userStoryboard.instantiateViewController(withIdentifier: "userRoot")
        } else {
            controller = userStoryboard.instantiateViewController(withIdentifier: "registrationController")
        }
        
        
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc(collectionView:didSelectItemAtIndexPath:) func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.performSegue(withIdentifier: "showRestaurants", sender: self)
        }
    }
}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(3, 0, 3, 0)
    }
}


extension MainViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? MainPageCell
        
        if cell == nil {
            cell = Bundle.main.loadNibNamed(reuseIdentifier, owner: self, options: nil)?[0] as? MainPageCell
        }
        
        let localizableKey = self.categories[indexPath.row]["title"]!
        
        cell?.titleLabel.text = LocalizableStrings.getString(key: localizableKey)
        
        cell?.imageView.image = UIImage(named: categories[indexPath.row]["image"]!)
        
        return cell!
    }
}







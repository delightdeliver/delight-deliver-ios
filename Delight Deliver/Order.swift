//
//  Order.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 20/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import Foundation

enum PaymentType {
    case Cash
    case Card
}


enum OrderStatus: Int {
    case processing = 0
    case registration = 1
    case cooking = 2
    case qualityCheck = 3
    case delivery = 4
    case closed = 5
    
    var str: String {
        switch self {
        case .processing:
            return "processing"
        case .registration:
            return "registration"
        case .cooking:
            return "cooking"
        case .qualityCheck:
            return "quality_check"
        case .delivery:
            return "delivery"
        default:
            return "closed"
        }
    }
    
    var title: String {
        return NSLocalizedString("order_status_\(self.str)", comment: "")
    }
}

class Order {
    let orderId: Int
    let comment: String
    let orderSum: Int
    let phone: String?
    let acceptedTime: String
    let restaurant: Restaurant
    var dishes: [Dish : Int] = [:]
    let paymentType: PaymentType
    var status: OrderStatus
    
    static var currentOrder: Order?
    
    
    init(orderId: Int, comment: String?, orderSum: Int, phone: String?, acceptedTime: String, restaurant: Restaurant, paymentType: PaymentType?, status: OrderStatus) {
        self.orderId = orderId
        
        if comment != nil {
            self.comment = comment!
        } else {
            self.comment = ""
        }
        
        self.orderSum = orderSum
        self.phone = phone
        self.acceptedTime = acceptedTime
        self.restaurant = restaurant
        
        if paymentType != nil {
            self.paymentType = paymentType!
        } else {
            self.paymentType = .Cash
        }
        
        self.status = status
    }
    
    func addDish(dish: Dish) -> Void {
        if self.dishes[dish] != nil {
            self.dishes[dish]! += 1
        } else {
            self.dishes[dish] = 1
        }
    }
}

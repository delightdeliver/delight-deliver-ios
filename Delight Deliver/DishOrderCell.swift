//
//  DishOrderCell.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 19/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

protocol DishOrderCellDelegate {
    func didAddDish(dish: Dish) -> Void
    
    func didRemoveDish(dish: Dish) -> Void
}

class DishOrderCell: UITableViewCell {

    @IBOutlet weak var dishImageView: UIImageView!
    @IBOutlet weak var dishNameLabel: UILabel!
    @IBOutlet weak var dishDescriptionLabel: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var dishAmountLabel: UILabel!
    @IBOutlet weak var dishPriceLabel: UILabel!
    
    
    var delegate: DishOrderCellDelegate?
    
    
    var dish: Dish! {
        didSet {
            self.dishNameLabel.text = self.dish.dishName.capitalizingFirstLetter()
            if let dishDescription = self.dish.dishDescription {
                self.dishDescriptionLabel.text = dishDescription
            }
            
            self.dishPriceLabel.text = "\(dish.price!) R"
        }
    }
    
    var amount: Int = 0 {
        didSet {
            let zeroAmount = amount == 0
            stepper.value = Double(amount)
            
            self.buyButton.isHidden = !zeroAmount
            self.stepper.isHidden = zeroAmount
            self.dishAmountLabel.isHidden = zeroAmount
            
            self.dishAmountLabel.text = "\(amount)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.buyButton.layer.cornerRadius = 5.0
        self.buyButton.layer.masksToBounds = true
        
        self.buyButton.isHidden = false
        self.stepper.isHidden = true
        self.dishAmountLabel.isHidden = true
        
        
//        self.dishImageView.layer.cornerRadius = self.dishImageView.frame.width / 2
//        self.dishImageView.layer.masksToBounds = true
    }
    
    @IBAction func stepperChanged(_ sender: AnyObject) {
        let newValue: Int = Int(stepper.value)
        
        if newValue < amount {
            amount -= 1
            delegate?.didRemoveDish(dish: self.dish)
        } else {
            amount += 1
            delegate?.didAddDish(dish: self.dish)
        }
    }
    
    @IBAction func buyPressed(_ sender: AnyObject) {
        self.amount = 1
        
        delegate?.didAddDish(dish: self.dish)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

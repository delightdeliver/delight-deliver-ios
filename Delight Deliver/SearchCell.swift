//
//  SearchCell.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 24/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
    
    // Restaurant outlets
    
    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var restaurantCuisineLabel: UILabel!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var ratingLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  RestaurantMenuDataViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 19/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

protocol RestaurantMenuDataViewControllerDelegate {
    func didUpdatedDishes(dishes: [Dish : Int], category: String) -> Void
    func didSelectDish(dish: Dish, index: Int) -> Void
}

class RestaurantMenuDataViewController: UITableViewController {
    
    var dishes: [Dish] = []
    var category: String!
    var restaurant: Restaurant!
    
    var delegate: RestaurantMenuDataViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "DishOrderCell", bundle: nil), forCellReuseIdentifier: "DishOrderCell")
        self.tableView.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        self.delegate?.didSelectDish(dish: self.dishes[indexPath.row], index: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DishOrderCell", for: indexPath) as! DishOrderCell
        let dish = self.dishes[indexPath.row]
        
        cell.dish = dish
        cell.amount = 0
        
        if let picture = dish.picture {
            if let url = URL(string: picture) {
                cell.dishImageView.af_setImage(withURL: url)
            }
        }
        
        cell.dishImageView.layer.cornerRadius = cell.dishImageView.frame.size.width / 2
        cell.dishImageView.layer.masksToBounds = true
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dishes.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Cell.heightes["DishOrderCell"]!
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }

}

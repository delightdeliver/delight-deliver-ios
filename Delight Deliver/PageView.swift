//
//  PageView.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 19/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import Foundation
import UIKit

protocol PageViewDelegate {
    func setImage(for page: Int)
    func didChanged(_ page: Int)
}


class PageView: UIView {
    var currentPage: Int!
    var scrollView: UIScrollView!
    var numberOfPages: Int!
    var delegate: PageViewDelegate?
    var imageViews: [UIImageView]!
    var grayViews: [UIView]!
    var padding: CGFloat = 30
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        return self.scrollView != nil && self.point(inside: point, with: event) ? self.scrollView : nil
    }
    
    var currentPageIndex: Int {
        get {
            return Int(self.scrollView.contentOffset.x / self.scrollView.frame.width)
        }
        set(idx) {
            let x: CGFloat = self.scrollView.frame.width * CGFloat(idx)
            
            self.scrollView.contentOffset = CGPoint(x: x, y: 0)
            self.currentPage = idx
        }
    }
    
    var contentSize: CGSize {
        get {
            return CGSize(width: self.scrollView.frame.width * CGFloat(self.numberOfPages), height: self.scrollView.frame.height)
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setupPageView() {
        self.imageViews = []
        self.grayViews = []
        
        self.layoutIfNeeded()
        
        self.setupScrollView()
        self.setupImageViews()
        self.updateGrayViews()
    }
    
    func setupScrollView() {
        self.scrollView = UIScrollView()
        self.scrollView.backgroundColor = UIColor.clear
        self.scrollView.delegate = self
        self.scrollView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(self.scrollView)
        
        
        let topContstraint = NSLayoutConstraint(item: self.scrollView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let bottomContstraint = NSLayoutConstraint(item: self.scrollView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingContraint = NSLayoutConstraint(item: self.scrollView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let trailingContraint = NSLayoutConstraint(item: self.scrollView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        
        
        self.addConstraints([leadingContraint, trailingContraint, topContstraint, bottomContstraint])
        
        self.scrollView.isOpaque = false
        self.scrollView.showsHorizontalScrollIndicator = false
        self.scrollView.showsVerticalScrollIndicator = false
        self.scrollView.isPagingEnabled = true
        self.scrollView.clipsToBounds = false
        self.scrollView.layoutIfNeeded()
        self.clipsToBounds = false
    }
    
    func addImageView(at index: Int) {
        let imageView = UIImageView()
        let grayView = UIView()
        grayView.backgroundColor = UIColor(white: 0.2, alpha: 0.9)
        self.imageViews.append(imageView)
        self.grayViews.append(grayView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        grayView.translatesAutoresizingMaskIntoConstraints = false
        self.scrollView.addSubview(imageView)
        self.scrollView.addSubview(grayView)
        
        
        
        let widthImage = NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .lessThanOrEqual, toItem: self.scrollView, attribute: .width, multiplier: 1, constant: 0)
        let heightImage = NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: self.scrollView, attribute: .height, multiplier: 1, constant: 0)
        let centerXImage = NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self.scrollView, attribute: .centerX, multiplier: CGFloat(1 + 2 * index), constant: 0)
        let centerYImage = NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: self.scrollView, attribute: .centerY, multiplier: 1, constant: 0)
        
        
        let grayTop = NSLayoutConstraint(item: grayView, attribute: .top, relatedBy: .equal, toItem: imageView, attribute: .top, multiplier: 1, constant: 0)
        let grayBottom = NSLayoutConstraint(item: grayView, attribute: .bottom, relatedBy: .equal, toItem: imageView, attribute: .bottom, multiplier: 1, constant: 0)
        let grayLeading = NSLayoutConstraint(item: grayView, attribute: .leading, relatedBy: .equal, toItem: imageView, attribute: .leading, multiplier: 1, constant: 0)
        let grayTrailing = NSLayoutConstraint(item: grayView, attribute: .trailing, relatedBy: .equal, toItem: imageView, attribute: .trailing, multiplier: 1, constant: 0)
        
        
        self.scrollView.addConstraints([widthImage, heightImage, centerXImage, centerYImage, grayTop, grayBottom, grayLeading, grayTrailing])
        
        
        imageView.setCornerRadius(5.0)
        grayView.setCornerRadius(5.0)
        
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
    }
    
    func updateGrayViews() {
        for grayView in self.grayViews {
            grayView.alpha = 0.9
        }
        
        let currentIndex = self.currentPageIndex
        self.grayViews[currentIndex].alpha = 0.0
    }
    
    func setupImageViews() {
        for i in 0..<self.numberOfPages {
            self.addImageView(at: i)
            self.delegate?.setImage(for: i)
        }
        
        let w = self.scrollView.frame.width
        let h = self.scrollView.frame.height
        
        self.scrollView.contentSize = CGSize(width: w * CGFloat(numberOfPages), height: h)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if self.scrollView != nil {
            self.scrollView.contentSize = self.contentSize
            self.currentPageIndex = self.currentPage
        }
    }
}

extension PageView: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.currentPage = self.currentPageIndex
        delegate?.didChanged(self.currentPage)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x, y: 0)
        
        let currentIndex = self.currentPageIndex
        let maxOffset: CGFloat = self.scrollView.frame.width
        let currentOffset = self.scrollView.contentOffset.x.truncatingRemainder(dividingBy: maxOffset)
        
        
        guard currentOffset != maxOffset && currentOffset != 0 else {
            return
        }
        
        guard currentIndex < self.numberOfPages - 1 else {
            return
        }
        
        let currentGrayView = self.grayViews[currentIndex]
        let nextGrayView = self.grayViews[currentIndex + 1]
        
        currentGrayView.alpha = 0.8 * currentOffset / maxOffset
        nextGrayView.alpha = 0.8 * (1 - (currentOffset / maxOffset))
    }
}















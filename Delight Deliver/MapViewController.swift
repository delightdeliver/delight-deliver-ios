//
//  MapViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 16/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit
import MapKit


class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet var restaurantView: UIView!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var restaurantCuisineLabel: UILabel!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var ratingNum: UILabel!
    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var minSumLabel: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    
    var selectedRestaurant: Restaurant!
    
    let locationManager = CLLocationManager()
    var regionRadius: CLLocationDistance = 1000
    
    var rests: [Restaurant] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.locationManager.delegate = self
        self.mapView.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        self.mapView.showsUserLocation = true
        
        if let location = locationManager.location {
            centerMapOnLocation(location: location)
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.view.addSubview(restaurantView)
        
        restaurantView.frame = CGRect(x: 0, y: self.view.frame.height,
                                      width: restaurantView.frame.width, height: restaurantView.frame.height)
        
        loadRestaurants()
    }
    
    func loadRestaurants() {
        self.rests = RestaurantHelper.instance.restaurants
        self.updateMap()
    }
    
    func updateMap() {
        for annotation: MKAnnotation in mapView.annotations {
            mapView.removeAnnotation(annotation)
        }
        
        for rest in self.rests {
            for i in 0..<rest.addressesCoordinates.count {
                let concrete_rest = ConcreteRestaurant(restaurant: rest, coordinateIndex: i)
                mapView.addAnnotation(concrete_rest)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        let reuseId = "RestaurantAnnotation"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView?.image = Images.mapImage
            anView?.canShowCallout = true
        } else {
            anView?.annotation = annotation
        }
        return anView
    }
    
    func setupRestaurantView() {
        self.restaurantImageView.af_setImage(withURL: URL(string: selectedRestaurant.picture!)!)
        self.restaurantNameLabel.text = selectedRestaurant.name.capitalizingFirstLetter()
        self.restaurantCuisineLabel.text = selectedRestaurant.cuisines.joined(separator: ", ").capitalizingFirstLetter()
        self.ratingControl.rating = selectedRestaurant.rating
        self.minSumLabel.text = "\(selectedRestaurant.minimumSum!) R"
        
        if let delSum = selectedRestaurant.deliveryCost {
            deliveryLabel.text = "\(delSum) R"
        } else {
            deliveryLabel.text = "Free"
        }
        
        self.ratingNum.text = ""
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let an = view.annotation as? ConcreteRestaurant {
            
            self.selectedRestaurant = an.restaurant
            setupRestaurantView()
            
            UIView.animate(withDuration: 0.5, animations: {
                view.image = Images.mapImageSelected
                
                self.restaurantView.frame = CGRect(x: 0, y: self.view.frame.height - self.restaurantView.frame.height - self.tabBarController!.tabBar.frame.height, width: self.restaurantView.frame.width, height: self.restaurantView.frame.height)
            })
        }
    }
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if let _ = view.annotation as? ConcreteRestaurant {
            
            self.selectedRestaurant = nil
            
            UIView.animate(withDuration: 0.5, animations: {
                view.image = Images.mapImage
                
                self.restaurantView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.restaurantView.frame.width, height: self.restaurantView.frame.height)
            })
        }
    }
    
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
}

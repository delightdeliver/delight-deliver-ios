//
//  NetworkHelper.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 16/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit
import Alamofire

class NetworkHelper: NSObject {
    
    enum ResponseType {
        case json, text
    }
    
    let baseUrl: URL = URL(string:"https://2-dot-delightdelivery2015.appspot.com")!
    let defaults: UserDefaults = UserDefaults.standard
    let cookieStorage: HTTPCookieStorage = HTTPCookieStorage.shared
    
    var allDishesNames: [String] = []
    var allCuisinesNames: [String] = []
    var allCategoriesNames: [String] = []
    var allIngredientsNames: [String] = []
    
    
    class var instance: NetworkHelper {
        struct Singleton {
            static let instance = NetworkHelper()
        }
        return Singleton.instance
    }
    
    func removeCookie() -> Void {
        defaults.removeObject(forKey: "COOKIE")
        
        for cookie in cookieStorage.cookies! {
            cookieStorage.deleteCookie(cookie)
        }
    }
    
    func getCookie() -> String? {
        return defaults.value(forKey: "COOKIE") as? String
    }
    
    func saveCookie(cookie: String) {
        defaults.setValue(cookie, forKeyPath: "COOKIE")
    }
    
    func prepareApp(success:@escaping ()->Void, failure:@escaping ()->Void) {
        // First – get all dishes names
        self.request(apiMethod: "/api/app_info", method: .get, parameters: nil, success: { data, headers, status in
            do {
                guard let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any] else {
                    return
                }
                
                if let cuisines = json["cuisines"] as? [String] {
                    self.allCuisinesNames = cuisines
                }
                if let dishNames = json["dish_names"] as? [String] {
                    self.allDishesNames = dishNames
                }
                if let categories = json["categories"] as? [String] {
                    self.allCategoriesNames = categories
                }
                if let ingredients = json["ingredients"] as? [String] {
                    self.allIngredientsNames = ingredients
                }
                
                success()
                
            } catch {
                failure()
            }
            
        }, failure: { _ in
            failure()
        })
    }
    
    
    func request(apiMethod: String, method:HTTPMethod, parameters: [String:Any]?, success:@escaping (_ data:Data?, _ headers: HTTPHeaders, _ statusCode:Int) -> Void, failure:@escaping (_ error: Error) -> Void) {
        
        guard let url = URL(string: apiMethod, relativeTo: baseUrl) else {
            return
        }
        
        var headers: HTTPHeaders = [:]
        
        if let cookies = self.getCookie() {
            headers["Cookie"] = cookies
        }
        
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).response { dataResponse in
            
            let data = dataResponse.data
            let error = dataResponse.error
            let headers = dataResponse.response?.allHeaderFields
            let code = dataResponse.response?.statusCode
            
            if error != nil {
                failure(error!)
            } else {
                success(data, headers as! HTTPHeaders, code!)
            }
        }
        
    }
    
    
    func loadDishes(name: String?, cal: Int?, price: Int?, cuisine: String?, restaurantId: Int?, category: String?, success:((_ dishes:[Dish])->Void)?, failure: (()->Void)?) {
        
        let _ = [
            "name": name ?? ""
//            "cal": cal,
//            "price": price,
//            "cuisine": cuisine,
//            "restaurant_id": restaurantId,
//            "category": category
        ]
        
        self.request(apiMethod: "/api/dish_search?name=\(name!)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: .get, parameters: nil, success: { data, headers, status in
            
            do {
                
                var dishes: [Dish] = []
                
                guard let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any] else {
                    return
                    
                }
                
                if let dishArr = json["results"] as? [NSDictionary] {
                    for dishDict in dishArr {
                        dishes.append(Dish(dishDict: dishDict)!)
                    }
                    if let success = success {
                        success(dishes)
                    }
                }
                
            } catch {
                print("Error1")
            }
            
        }, failure: { error in
            print(error)
        })
    }
    
    func loadRestaurants(success:@escaping() -> Void) {
        self.request(apiMethod: "/api/restaurant_search", method: .get, parameters: nil, success: { data, headers, status in
            do {
                guard let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String:Any]] else {
                    return
                }
                for rest_dict in json {
                    let _ = Restaurant(restaurantDict: rest_dict)
                }
                success()
            } catch { }
        }, failure: { error in })
    }
    
    func getUserInfo(success:@escaping (_ loggedIn:Bool) -> Void, failure:@escaping (_ error:DDError) -> Void) {
        self.request(apiMethod: "/api/user_info", method: .post, parameters: nil, success: { data, headers, statusCode in
            
            do {
                guard let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any] else {
                    failure(.networkError)
                    return
                }
                guard let status = json["status"] as? String else {
                    failure(.networkError)
                    return
                }
                
                if status == "FAILURE" {
                    success(false)
                } else {
                    User.setUser(dictionary: json)
                    success(true)
                }
                
            } catch {
                failure(.networkError)
            }
            
        }, failure: { error in
            failure(.networkError)
        })
    }
    
    
    func setUserInfo(name: String, surname: String, email: String, phone: String, addresses: [Address]?, success:(()->Void)?, failure:((_ error:DDError)->Void)?) {
        
        var params: [String:Any] = [
            "name": name,
            "surname": surname,
            "email": email,
            "phone": phone
        ]
        
        if let addresses = addresses {
            let addressesArray = addresses.map { $0.toDictionary()}
            params["addresses"] = addressesArray
        } else if let uAddresses = User.user?.addresses {
            params["addresses"] = uAddresses.map{ $0.toDictionary() }
        }
        
        
        self.request(apiMethod: "/api/info_add", method: .post, parameters: params, success: { data, headers, statusCode in
            do {
                guard let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any] else {
                    if let failure = failure {
                        failure(.networkError)
                    }
                    return
                }
                guard let status = json["status"] as? String else {
                    if let failure = failure {
                        failure(.networkError)
                    }
                    return
                }
                
                if status == "FAILURE" {
                    if let failure = failure {
                        failure(.networkError)
                    }
                    return
                } else {
                    if let success = success {
                        success()
                    }
                }
            } catch {
                if let failure = failure {
                    failure(.networkError)
                }
            }
            
            }, failure: { error in
                if let failure = failure {
                    failure(.networkError)
                }
        })
    }
    
    func logout(success: (()->Void)?, failure: (() -> Void)?) {
        self.request(apiMethod: "/api/logout", method: .post, parameters: nil, success: { data, headers, statusCode in
            
            self.removeCookie()
            User.removeUser()
            
            if let success = success {
                success()
            }
            }, failure: { err in
        })
        
        
    }
    
    func rateDish(dishId: Int, rating: Int, comment: String?, success: (()->Void)?, failure: (() -> Void)?) {
        
        var params: [String:Any] = [
            "ID": dishId,
            "rating": rating,
        ]
        
        if let comment = comment {
            params["comment"] = comment
        }
        
        self.request(apiMethod: "/api/dish_rate", method: .post, parameters: params, success: { data, headers, code in
            if let success = success {
                success()
            }
            }, failure: { error in
                if let failure = failure {
                    failure()
                }
        })
        
    }
    
    
    func rateRestaurant(restaurantId: Int, rating: Int, comment: String?, success: (()->Void)?, failure: (() -> Void)?) {
        
        var params: [String:Any] = [
            "ID": restaurantId,
            "rating": rating,
        ]
        if let comment = comment {
            params["comment"] = comment
        }
        
        self.request(apiMethod: "/api/restaurant_rate", method: .post, parameters: params, success: { data, headers, code in
            if let success = success {
                success()
            }
        }, failure: { error in
            if let failure = failure {
                failure()
            }
        })
        
    }
    
    
    func loadOrderHistory(success:@escaping (_ orders: [Order])->Void, failure: (() -> Void)?) {
        
        self.request(apiMethod: "/api/order_check", method: .post, parameters: nil, success: { data, headers, statusCode in
            
            do {
                guard let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any] else {
                    if let failure = failure {
                        failure()
                    }
                    return
                }
                guard let status = json["status"] as? String else {
                    if let failure = failure {
                        failure()
                    }
                    return
                }
                
                if status == "FAILURE" {
                    if let failure = failure {
                        failure()
                    }
                } else {
                    if let result = json["result"] as? NSArray {
                        var orders: [Order] = []
                        
                        for order in result {
                            if let order = order as? NSDictionary {
                                
                                let comment = order["comment"] as? String
                                let orderSum = order["order_sum"] as! Int
                                let time = order["accepted_time"] as! String
                                let phone = order["phone"] as? String
                                
                                var paymentType: PaymentType = .Cash
                                
                                if let isByCard = order["payment_by_card"] as? Bool {
                                    if isByCard {
                                        paymentType = .Card
                                    }
                                }
                                
                                let arrayEl = (order["array"] as! NSArray)[0] as! NSDictionary
                                
                                let orderId = arrayEl["order_id"] as! Int
                                let dishesArray = arrayEl["dishes"] as! NSArray
                                
                                let restaurant = Restaurant(restaurantDict: arrayEl["restaurant"] as! [String:Any])
                                
                                var orderStatus: OrderStatus = .processing
                                
                                if let statusInt = order["order_status"] as? Int {
                                    if let status = OrderStatus(rawValue: statusInt) {
                                        orderStatus = status
                                    }
                                }
                                
                                let order = Order(orderId: orderId, comment: comment, orderSum: orderSum, phone: phone, acceptedTime: time, restaurant: restaurant, paymentType: paymentType, status: orderStatus)
                                
                                
                                for d in dishesArray {
                                    let dish = Dish(dishDict: d as! NSDictionary)
                                    if dish != nil {
                                        order.addDish(dish: dish!)
                                    }
                                }
                                
                                
                                orders.append(order)
                            }
                        }
                        
                        success(orders)
                    }
                }
                
            } catch {
                if let failure = failure {
                    failure()
                }
                
            }
            
            
            }, failure: { error in
                if let failure = failure {
                    failure()
                }
        })
    }
    
    func login(email: String, password: String, success:@escaping () -> Void, failure:@escaping (_ error:DDError)->Void) {
        let params = [
            "email": email,
            "password": password
        ]
        
        self.request(apiMethod: "/api/login", method: .post, parameters: params, success: { data, headers, statusCode in
            
            if let dataString = String(data: data!, encoding: .utf8) {
                if dataString == "You already login" {
                    failure(.alreadyLoginError)
                    return
                }
            }
            
            if let cookie = headers["Set-Cookie"] {
                self.saveCookie(cookie: cookie)
            }
            
            do {
                guard let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any] else {
                    failure(.networkError)
                    return
                }
                guard let status = json["status"] as? String else {
                    failure(.networkError)
                    return
                }
                
                
                guard status != "FAILURE" else {
                    failure(.loginError)
                    return
                }
                
                var userDict = json
                
                userDict["email"] = email
                
                User.setUser(dictionary: userDict)
                success()
            } catch {
                failure(.networkError)
            }
            
            
        }, failure: { error in
            failure(.networkError)
        })
    }
}






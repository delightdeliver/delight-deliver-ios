//
//  PreorderCell.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 19/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.

import UIKit

protocol PreorderCellDelegate {
    func didSelectNumberOfPeople(number: Int)
    func didSetDateAndTime(newDate: Date, newDateStr: String, newTimeStr: String)
    func didSetPersonValue(key: String, value: String)
    func didSetComment(comment: String)
    func didPlaceOrder()
    func didRemoveRestaurant()
}


enum PreorderCellType: String {
    case Name = "name"
    case Surname = "surname"
    case Phone = "phone"
    case Comment = "comment"
    case DateAndTime = "date_and_time"
    case NumberOfPerson = "number_of_person"
    case Dish = "dish"
    
    var title: String {
        return LocalizableStrings.getString(key: "preorder_\(self.rawValue)_title")
    }
}

class PreorderCell: UITableViewCell {
    
    // Title Empty outlets
    @IBOutlet weak var emptySelectLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    // Title selected outlets
    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var restaurantAddressLabel: UILabel!
    @IBOutlet weak var dismissRestaurantButton: UIButton!
    
    // Regular outlets
    @IBOutlet weak var regularTitleLabel: UILabel!
    
    // People number outlets
    @IBOutlet weak var peoplePickerView: UIPickerView!
    
    // Time outlets
    @IBOutlet weak var datePicker: UIDatePicker!
    
    // Comment outlets
    @IBOutlet weak var commentTextView: UITextView!
    
    // User info outlets
    @IBOutlet weak var personInfoTitleLabel: UILabel!
    @IBOutlet weak var personInfoTextField: UITextField!
    
    // Button outlets
    @IBOutlet weak var makeReservationButton: UIButton!
    
    
    var delegate: PreorderCellDelegate?
    let pickerData = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    var cellType: PreorderCellType!
    
    
    @IBAction func makeReservation(_ sender: AnyObject) {
        delegate?.didPlaceOrder()
    }
    
    @IBAction func dismissRestaurant(_ sender: AnyObject) {
        delegate?.didRemoveRestaurant()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
        if makeReservationButton != nil {
            makeReservationButton.layer.cornerRadius = 5.0
            makeReservationButton.layer.masksToBounds = true
        }
        if self.datePicker != nil {
            let now = Date()
            self.datePicker.minimumDate = now
        }
        if self.peoplePickerView != nil {
            self.peoplePickerView.delegate = self
            self.peoplePickerView.dataSource = self
        }
        if self.commentTextView != nil {
            self.commentTextView.delegate = self
        }
        
        if self.personInfoTextField != nil {
            self.personInfoTextField.delegate = self
        }
    }
    @IBAction func setDateAndTime(_ sender: AnyObject) {
        let date = datePicker.date
        //string_date = "28-09-2015 20:30"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateString = dateFormatter.string(from: date)
        
        dateFormatter.dateFormat = "hh:mm"
        let timeString = dateFormatter.string(from: date)
        
        self.delegate?.didSetDateAndTime(newDate: date, newDateStr: dateString, newTimeStr: timeString)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}


extension PreorderCell: UITextViewDelegate {
    // TextView is only comment cell, so calling it
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = PreorderCellType(rawValue: "comment")!.title
            textView.textColor = UIColor.lightGray
        } else {
            delegate?.didSetComment(comment: textView.text)
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
}

extension PreorderCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text {
            delegate?.didSetPersonValue(key: cellType.rawValue, value: text)
        }
        textField.endEditing(true)
        return true
    }
}

extension PreorderCell: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        delegate?.didSelectNumberOfPeople(number: pickerData[row])
    }
}

extension PreorderCell: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return "1 person"
        } else {
            return "\(pickerData[row]) persons"
        }
    }
}



extension PreorderCell {
    override func expand() {
    }
    override func collapse() {
    }
}


//
//  String+isEmail.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 23/01/16.
//  Copyright © 2016 temi4hik. All rights reserved.
//

import Foundation


extension String {
    func isEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        
        return emailTest.evaluate(with: self)
    }
}

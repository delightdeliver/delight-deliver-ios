//
//  Dish.swift
//  Delight Deliver
//
//  Created by Artem on 25/12/15.
//  Copyright © 2015 temi4hik. All rights reserved.
//

import UIKit

func ==(lhs: Dish, rhs: Dish) -> Bool {
    return lhs.id == rhs.id
}

class Dish: Hashable {
    
    var hashValue: Int {
        get {
            return id
        }
    }
    
    var restaurantId: Int!
    var id: Int!
    var dishName: String!
    var price: Int!
    var category: String?
    var picture: String?
    var rating: Int?
    var dishDescription: String?
    var ingridients: [String]?
    var tags: [String]?
    var cal: Int?
    var cuisine: String?
    
    
    var tagsWithSharp: String {
        
        get {
            var arr: [String] = []
            for tag in self.tags! {
                arr += ["#\(tag)"]
            }
            return arr.joined(separator: ", ")
        }
    }
    
    var restaurant: Restaurant? {
        get {
            return RestaurantHelper.instance.getRestaurant(id: self.restaurantId)
        }
    }
    
    init?(dishDict: NSDictionary) {
        // Setting required fields. If some of them are nil – unable to create object
        
        guard let
            restaurantId = dishDict["restaurant_ID"] as? Int,
            let dishId = dishDict["ID"] as? Int,
            let name = dishDict["name"] as? String,
            let price = dishDict["price"] as? Int
        else {
            return nil
        }
        
        self.restaurantId = restaurantId
        self.id = dishId
        self.dishName = name
        self.price = price
        
        
        // Getting optional properties
        if let category = dishDict["category"] as? String {
            self.category = category
        } else {
            self.category = nil
        }
        if let picture = dishDict["picture"] as? String {
            self.picture = picture
        } else {
            self.picture = nil
        }
        if let rating = dishDict["rating"] as? Float {
            self.rating = Int(rating)
        } else {
            self.rating = nil
        }
        if let description = dishDict["description"] as? String {
            self.dishDescription = description
        } else {
            self.dishDescription = nil
        }
        if let ingridients = dishDict["ingredients"] as? [String] {
            self.ingridients = ingridients
        } else {
            self.ingridients = nil
        }
        if let tags = dishDict["tags"] as? [String] {
            self.tags = tags
        } else {
            self.tags = nil
        }
        if let cal = dishDict["cal"] as? Int {
            self.cal  = cal
        } else {
            self.cal = nil
        }
        if let cuisine = dishDict["cuisine"] as? String {
            self.cuisine = cuisine
        } else {
            self.cuisine = nil
        }
        
        DishHelper.instance.addDish(dish: self)
    }

}

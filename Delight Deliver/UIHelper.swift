//
//  Sizes.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 16/07/2016.
//  Copyright © 2016 Artem Ryabukha. All rights reserved.
//

import Foundation


import Foundation
import UIKit


struct Ratio {
    static let mainPageIpadRatio: CGFloat = 1.0142
    static let mainPageIphoneRatio: CGFloat = 1.3522
}


struct ScreenSize {
    static let screenWidth = UIScreen.main.bounds.size.width
    static let screenHeight = UIScreen.main.bounds.size.height
    static let maxScreenLength = max(ScreenSize.screenWidth, ScreenSize.screenHeight)
    static let minScreenLength = min(ScreenSize.screenWidth, ScreenSize.screenHeight)
}

struct DeviceType {
    static let iPhone4OrLess = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxScreenLength < 568.0
    static let iPhone5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxScreenLength == 568.0
    static let iPhone6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxScreenLength == 667.0
    static let iPhone6Plus = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxScreenLength == 736.0
    
    static let iPad = UIDevice.current.userInterfaceIdiom == .pad
    static let iPhone = UIDevice.current.userInterfaceIdiom == .phone
    
}

struct DeviceOrientation {
    static var isPortrait: Bool {
        get {
            return UIApplication.shared.statusBarOrientation.isPortrait
        }
    }
    static var isLandscape: Bool {
        get {
            return UIApplication.shared.statusBarOrientation.isLandscape
        }
    }
    
    static func orientationForSize(size: CGSize) -> UIInterfaceOrientation {
        if size.width > size.height {
            return UIInterfaceOrientation.landscapeLeft
        } else {
            return UIInterfaceOrientation.portrait
        }
    }
}

struct SystemVersion {
    static func equal(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                              options: NSString.CompareOptions.numeric) == ComparisonResult.orderedSame
    }
    
    static func greaterThan(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                              options: NSString.CompareOptions.numeric) == ComparisonResult.orderedDescending
    }
    
    static func greaterThanOrEqual(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                              options: NSString.CompareOptions.numeric) != ComparisonResult.orderedAscending
    }
    
    static func lessThan(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                              options: NSString.CompareOptions.numeric) == ComparisonResult.orderedAscending
    }
    
    static func lessThanOrEqual(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                              options: NSString.CompareOptions.numeric) != ComparisonResult.orderedDescending
    }
}

//
//  LoginViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 20/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBAction func dismiss(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func navigateToRegistration(_ sender: AnyObject) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func login(_ sender: AnyObject) {
        NetworkHelper.instance.login(email: self.emailTextField.text!, password: self.passwordTextField.text!, success: {
            self.dismiss(animated: true, completion: nil)
            }, failure: { err in
                if err == .alreadyLoginError {
                    NetworkHelper.instance.getUserInfo(success: {_ in 
                        
                        }, failure: { err in
                            
                    })
                }
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.emailTextField.endEditing(true)
        self.passwordTextField.endEditing(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emailTextField.layer.cornerRadius = 5.0
        self.passwordTextField.layer.cornerRadius = 5.0
        
        self.emailTextField.layer.masksToBounds = true
        self.passwordTextField.layer.masksToBounds = true
        
        self.loginButton.layer.cornerRadius = 5.0
        self.loginButton.layer.masksToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

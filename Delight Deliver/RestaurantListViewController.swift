//
//  RestaurantListViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 18/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit
import AlamofireImage

protocol RestaurantListDelegate {
    func setRest(restaurant: Restaurant)
}


class RestaurantListViewController: UITableViewController {

    var loadingView: UIView!
    var indicator: UIActivityIndicatorView!
    let reuseIdentifier: String = "RestaurantCell"
    
    var restaurants: [Restaurant] = []
    var selectedRestaurant: Restaurant?
    
    var dict: NSMutableDictionary = NSMutableDictionary()
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var footerLabel: UILabel!
    
    var delegate: RestaurantListDelegate?
    
    var footerLabels: [UILabel] = []
    var hideFooter: Bool = false
    
    
    func showFilter() {
        self.performSegue(withIdentifier: "showFilterFromRestaurants", sender: self)
    }
    
    
    @IBAction func cleanFilters(_ sender: AnyObject) {
        self.dict = NSMutableDictionary()
        self.footerLabel.isHidden = false
        self.setupScrollView()
    }
    
    func setupScrollView() {
        for lab in footerLabels {
            lab.removeFromSuperview()
        }
        
        footerLabels = []
        
        self.scrollView.isScrollEnabled = true
        let spacing: CGFloat = 15
        var totalWidth: CGFloat = CGFloat(spacing)
        let totalHeight = self.footerView.frame.height
        let labelHeight: CGFloat = 40.0
        
        for (_, val) in dict {
            
            var value: String;
            
            if let valueArray = val as? [String] {
                value = valueArray.joined(separator: ", ")
            } else {
                value = "\(val)"
            }
            
            let label = UILabel()
            label.backgroundColor = UIColor.clear
            label.text = value
            
            let font = UIFont.boldSystemFont(ofSize: 13)
            label.font = font
            label.layer.borderWidth = 1.0
            label.layer.borderColor = UIColor.white.cgColor
            label.layer.cornerRadius = 20
            label.layer.masksToBounds = false
            label.textColor = UIColor.white
            label.textAlignment = .center
            
            
            let labelWidth = label.text!.characters.count * 10 + 25
            let labelFrame = CGRect(x: totalWidth, y: (totalHeight - labelHeight) / 2, width: CGFloat(labelWidth), height: labelHeight)
            
            label.frame = labelFrame
            
            totalWidth += labelFrame.width + spacing
            footerLabels.append(label)
            self.scrollView.addSubview(label)
        }
        scrollView.contentSize = CGSize(width: totalWidth, height: totalHeight)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if dict.count > 0 {
            self.footerLabel.isHidden = true
        } else {
            self.footerLabel.isHidden = false
        }
        
        self.setupScrollView()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let touch = UITapGestureRecognizer(target: self, action: #selector(showFilter))
        touch.numberOfTapsRequired = 1
        self.footerView.addGestureRecognizer(touch)
        
        self.tableView.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        
        self.restaurants = RestaurantHelper.instance.restaurants
        tableView.reloadData()
        self.footerView.isHidden = false
        
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showFilterFromRestaurants" {
            let destination = segue.destination as! FilterViewController
            destination.parentController = self
            destination.topBarEnabled = false
            destination.dishDict = self.dict
        } else if segue.identifier == "showRestaurantFromList" {
            let destination = segue.destination as! RestaurantViewController
            destination.restaurant = self.selectedRestaurant!
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return hideFooter ? nil : footerView
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedRestaurant = restaurants[indexPath.row]
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if delegate == nil {
            self.performSegue(withIdentifier: "showRestaurantFromList", sender: self)
        } else {
            delegate?.setRest(restaurant: self.selectedRestaurant!)
            let _ = self.navigationController?.popViewController(animated: true)
        }
        
        
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.restaurants.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return hideFooter ? 0.1 : 54.0
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 174
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! RestaurantCell
        
        
        let restaurant = self.restaurants[indexPath.row]
        cell.restaurantNameLabel.text = restaurant.name.capitalizingFirstLetter()
        cell.ratingControl.rating = restaurant.rating
        
        
        cell.votesNumberLabel.text = ""
        cell.restaurantCuisineLabel.text = restaurant.cuisines.joined(separator: ", ")
        
        if restaurant.deliveryCost != nil {
            cell.restaurantDeliverySumLabel.text = "\(restaurant.deliveryCost!) R"
        } else {
            cell.restaurantDeliverySumLabel.text = "Free"
        }
        
        if restaurant.minimumSum != nil {
            cell.restaurantMinSumLabel.text = "\(restaurant.minimumSum!) R"
        }
        
        
        if let pic = restaurant.picture {
            if let url = NSURL(string: pic) {
                cell.restaurantImageView.af_setImage(withURL: url as URL)
            }
        }
        
        return cell
    }
}

//
//  RestaurantCell.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 18/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class RestaurantCell: UITableViewCell {

    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var restaurantCuisineLabel: UILabel!
    @IBOutlet weak var restaurantMinSumLabel: UILabel!
    @IBOutlet weak var restaurantDeliverySumLabel: UILabel!
    
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var votesNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.restaurantImageView.layer.masksToBounds = false
        self.restaurantImageView.layer.cornerRadius = self.restaurantImageView.frame.size.width / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  Strings.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 19/07/2016.
//  Copyright © 2016 Artem Ryabukha. All rights reserved.
//

import Foundation


enum DDError: String {
    case loginError = "login_error"
    case registrationError = "registration_error"
    case networkError = "network_error"
    
    case alreadyLoginError = "already_login_error"
    
    
    
    
    var title: String {
        return NSLocalizedString("\(self.rawValue)_title", comment: "")
    }
    var message: String {
        return NSLocalizedString("\(self.rawValue)_message", comment: "")
    }
}


struct LocalizableStrings {
    struct MainPage {
        static let restaurantTitle = NSLocalizedString("restaurant_title", comment: "Title for restaurant category");
        static let secondTitle = NSLocalizedString("second_title", comment: "Title for second dishes category");
        static let pastaTitle = NSLocalizedString("pasta_title", comment: "Title for pasta category");
        static let dessertTitle = NSLocalizedString("dessert_title", comment: "Title for dessert category");
    }
    
    struct Filter {
        static let forFirstTitle = NSLocalizedString("filter_for_first_title", comment: "")
        static let forSecondTitle = NSLocalizedString("filter_for_second_title", comment: "")
        
        static let dishTitle = NSLocalizedString("filter_dish_title", comment: "")
        static let cuisineTitle = NSLocalizedString("filter_cuisine_title", comment: "")
        static let ingredientsTitle = NSLocalizedString("filter_ingredients_title", comment: "")
        static let tagsTitle = NSLocalizedString("filter_tags_title", comment: "")
        
        static let sliderCaloriesTitle = NSLocalizedString("filter_calories_title", comment: "")
        static let sliderCaloriesText = NSLocalizedString("filter_calories_text", comment: "")
        
        static let sliderPriceTitle = NSLocalizedString("filter_price_title", comment: "")
        static let sliderPriceText = NSLocalizedString("filter_price_text", comment: "")
    }
    
    static func getString(key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
    
}

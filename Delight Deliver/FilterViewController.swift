//
//  FilterViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 16/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

enum SearchType {
    case OnePerson
    case TwoPeople
}

class FilterViewController: CustomTableViewController {
    var firstPersonDict: NSMutableDictionary = NSMutableDictionary()
    var secondPersonDict: NSMutableDictionary = NSMutableDictionary()
    
    var dictForTwo: NSMutableDictionary = NSMutableDictionary()
    var dishDict: NSMutableDictionary = NSMutableDictionary()
    
    var parentController: UIViewController?
    
    var topBarEnabled = true
    
    
    var searchType: SearchType = .OnePerson {
        didSet {
            switch searchType {
            case .OnePerson:
                self.descriptorPath = "FilterForOneDescriptor"
            case .TwoPeople:
                self.descriptorPath = "FilterForTwoDescriptor"
            }
        }
    }
    
    override func configureParams() {
        cellNibs = ["TopFilterCell", "TitleFilterCell", "RegularFilterCell","RegularSelectionFilterCell", "TagsFilterCell", "SliderFilterCell", "ButtonFilterCell"]
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dictForTwo.setValue(firstPersonDict, forKey: "first")
        dictForTwo.setValue(secondPersonDict, forKey: "second")
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    
    func setUnselected(button: UIButton) {
        button.backgroundColor = UIColor(white: 1.0, alpha: 0.57)
    }
    
    func setSelected(button: UIButton) {
        button.backgroundColor = UIColor.white
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentCellDescriptor = getCellDescriptorForIndexPath(indexPath: indexPath)
        let cellIdentifier = currentCellDescriptor["cellIdentifier"] as! String
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FilterCell
        
        
        if self.searchType == .TwoPeople {
            cell.isFirstPerson = indexPath.section == 0
        } else {
            cell.isFirstPerson = nil
        }
        
        switch cellIdentifier {
        case "TopFilterCell":
            
            if !topBarEnabled {
                cell.dishButton.isHidden = true
                cell.restaurantButton.isHidden = true
                cell.searchLabel.isHidden = true
            }
            
            if self.searchType == .OnePerson {
                self.setSelected(button: cell.dishButton)
                self.setUnselected(button: cell.restaurantButton)
            } else {
                self.setUnselected(button: cell.dishButton)
                self.setSelected(button: cell.restaurantButton)
            }
            cell.selectionStyle = .none
            
            
            break
        case "TitleFilterCell":
            if let image = currentCellDescriptor["image"] as? String {
                cell.titleImageView.image = UIImage(named: image)
            }
            if let title = currentCellDescriptor["title"] as? String {
                cell.titleLabel.text = LocalizableStrings.getString(key: title)
            }
            
            let expanded = currentCellDescriptor["isExpanded"] as! Bool
            if expanded {
                cell.expand()
            } else {
                cell.collapse()
            }
            
            
            break
        case "RegularFilterCell":
            if let title = currentCellDescriptor["title"] as? String {
                cell.regularTitleLabel.text = LocalizableStrings.getString(key: title)
            }
            
            let cellType = currentCellDescriptor["type"] as! String
            
            cell.textCellType = TextCellType(rawValue: cellType)!
            
            switch self.searchType {
            case .OnePerson:
                if cell.textCellType == .Ingredients {
                    
                }
                break
            default:
                break
            }
            
            break
        case "RegularSelectionFilterCell":
            
            let cellType = currentCellDescriptor["type"] as! String
            
            cell.textCellType = TextCellType(rawValue: cellType)!
            
            switch cell.textCellType! {
            case .DishName:
                cell.items = NetworkHelper.instance.allCategoriesNames
                cell.selectionTableView.allowsMultipleSelection = false
                if let it = dishDict[cellType] as? String {
                    cell.selectedItem = it
                }
                break
            case .Ingredients:
                cell.items = NetworkHelper.instance.allIngredientsNames
                cell.selectionTableView.allowsMultipleSelection = true
                break
            default:
                cell.selectionTableView.allowsMultipleSelection = false
                cell.items = NetworkHelper.instance.allCuisinesNames
                
                if let it = dishDict[cellType] as? String {
                    cell.selectedItem = it
                }
                break
            }
            cell.selectionTableView.reloadData()
            
            break
        case "TagsFilterCell":
            if let title = currentCellDescriptor["title"] as? String {
                cell.titleLabel.text = LocalizableStrings.getString(key: title)
            }
            
            cell.tagsControl.loadTags()
            cell.tagsControl.checkable = true
            cell.selectionStyle = .none
            break
        case "SliderFilterCell":
            if let title = currentCellDescriptor["title"] as? String {
                cell.sliderTitleLabel.text = LocalizableStrings.getString(key: title)
            }
            if let minValue = currentCellDescriptor["minValue"] as? Float {
                cell.slider.minimumValue = minValue
            }
            if let maxValue = currentCellDescriptor["maxValue"] as? Float {
                cell.slider.maximumValue = maxValue
            }
            
            let cellType = currentCellDescriptor["type"] as! String
//            let slText = currentCellDescriptor["slText"] as! String
            
            cell.sliderCellType = SliderCellType(rawValue: cellType)
            cell.selectionStyle = .none
            
            let value: Int
            let valueStr: String?
            let key = cell.sliderCellType.rawValue
            
            switch self.searchType {
            case .OnePerson:
                valueStr = dishDict.object(forKey: key) as? String
                break
            default:
                if cell.isFirstPerson! {
                    valueStr = firstPersonDict[key] as? String
                } else {
                    valueStr = secondPersonDict[key] as? String
                }
                break
            }
            
            if valueStr != nil {
                value = Int(valueStr!)!
                cell.slider.value = Float(value)
                cell.sliderCurrentLabel.text = valueStr!
            } else {
                cell.slider.value = 0
                cell.sliderCurrentLabel.text = ""
            }
            
            
            break
        case "ButtonFilterCell":
            cell.findButton.layer.cornerRadius = 10.0
            cell.findButton.setTitle("", for: .normal)
            
            cell.findButton.setTitle("Found: \(0)", for: .normal)
            cell.activityIndicator.stopAnimating()
            
            cell.selectionStyle = .none
            break
        default:
            break
        }
        
        cell.delegate = self
        
        return cell
        
    }

}


extension FilterViewController: FilterCellDelegate {
    func didPressFindButton(parentCell: FilterCell) {
        if !topBarEnabled {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func didChangeRegularValue(newValues: [String], parentCell: FilterCell) {
        switch self.searchType {
        case .OnePerson:
            if parentCell.textCellType! == .Ingredients {
                dishDict["ingredients"] = newValues
            } else {
                dishDict[parentCell.textCellType!.rawValue] = newValues[0]
            }
            break
        default:
            break
        }
    }
    
    func didChangeSliderValue(newSliderValue: Int, parentCell: FilterCell) {
        
        let key = parentCell.sliderCellType.rawValue
        
//        let caloriesRow: Int
//        let priceRow: Int
//        let section: Int
        
        
        if searchType == .OnePerson {
            dishDict.setValue(String(newSliderValue), forKey: key)
//            section = 0
            // loadDishes()
            
        } else {
            if parentCell.isFirstPerson! {
//                section = 0
                firstPersonDict.setValue(String(newSliderValue), forKey: key)
            } else {
//                section = 1
                secondPersonDict.setValue(String(newSliderValue), forKey: key)
            }
            // loadRestaurants()
        }
        
//        caloriesRow = (cellDescriptors.object(at: section) as! NSArray).count - 2
//        priceRow = caloriesRow + 1
    }
    func didToggleTag(newTags: Set<String>, parentCell: FilterCell) {
        let tags = Array(newTags)
        
        if searchType == .OnePerson {
            dishDict["tags"] = tags
        } else {
            if parentCell.isFirstPerson! {
                firstPersonDict["tags"] = tags
            } else {
                secondPersonDict["tags"] = tags
            }
        }
    }
    
    
    func didChooseDish(parentCell: FilterCell) {
        guard descriptorPath != "FilterForOneDescriptor" else {
            return
        }
        
        descriptorPath = "FilterForOneDescriptor"
        
        setSelected(button: parentCell.dishButton)
        setUnselected(button: parentCell.restaurantButton)
        
        
        self.searchType = .OnePerson
        
        loadCellDescriptors()
    }
    
    func didChooseRestaurant(parentCell: FilterCell) {
        guard descriptorPath != "FilterForTwoDescriptor" else {
            return
        }
        
        descriptorPath = "FilterForTwoDescriptor"
        setUnselected(button: parentCell.dishButton)
        setSelected(button: parentCell.restaurantButton)
        
        self.searchType = .TwoPeople
        
        loadCellDescriptors()
    }
    
}

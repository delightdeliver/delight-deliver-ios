//
//  FilterItemCell.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 18/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import Foundation
import UIKit

class FilterItemCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkedImageView: UIImageView!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
        
        if selected {
//            self.titleLabel.textColor = UIColor(red: 28/255, green: 28/255, blue: 28/255, alpha: 1.0)
//            self.contentView.backgroundColor = UIColor(red: 255/255, green: 209/255, blue: 70/255, alpha: 0.64)
            self.checkedImageView.image = Images.greenCheck
        } else {
//            self.titleLabel.textColor = UIColor(red: 134/255, green: 134/255, blue: 134/255, alpha: 1.0)
//            self.contentView.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
            self.checkedImageView.image = nil
        }
        
        
    }
}

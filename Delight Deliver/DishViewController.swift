//
//  DishViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 19/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class DishViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageView: PageView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var dishLabel: UILabel!
    @IBOutlet weak var ingredientsLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    
    @IBOutlet weak var restaurantView: UIView!
    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var restaurantHeightConstraint: NSLayoutConstraint!
    
    var dishes: [Dish] = []
    var selectedDishIndex: Int!
    var restaurantHidden: Bool = true
    
    var restaurant: Restaurant {
        get {
            return dishes[selectedDishIndex].restaurant!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.pageView.delegate = self
        self.pageView.numberOfPages = self.dishes.count
        self.pageControl.numberOfPages = self.dishes.count
        
        if self.dishes.count == 1 {
            self.pageControl.isHidden = true
        }
        
        self.pageView.setupPageView()
        self.pageView.currentPageIndex = self.selectedDishIndex
        self.updateDishInfo(page: self.selectedDishIndex)
        self.pageView.updateGrayViews()
        
        if restaurantHidden {
            restaurantHeightConstraint.constant = 0
        } else {
            restaurantNameLabel.text = restaurant.name.capitalizingFirstLetter()
            restaurantImageView.af_setImage(withURL: URL(string: restaurant.picture!)!)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRestaurantFromDish" {
            let destination = segue.destination as! RestaurantViewController
            destination.restaurant = self.restaurant
        }
    }
    
    func updateDishInfo(page: Int) {
        
        self.selectedDishIndex = page
        
        let dish = self.dishes[page]
        self.dishLabel.text = dish.dishName.capitalizingFirstLetter()
        self.tagsLabel.text = dish.tagsWithSharp
        self.pageControl.currentPage = page
        self.priceLabel.text = "\(dish.price!) R"
        
        restaurantNameLabel.text = restaurant.name.capitalizingFirstLetter()
        restaurantImageView.af_setImage(withURL: URL(string: restaurant.picture!)!)
        
        if let ingredients = dish.ingridients {
            self.ingredientsLabel.text = ingredients.joined(separator: ", ")
        }
        if let cal = dish.cal {
            self.caloriesLabel.text = "\(cal) kcal"
        }
        
        if let category = dish.category {
            self.navigationItem.title = "\(category.capitalizingFirstLetter()) \(restaurant.name)"
        } else {
            self.navigationItem.title = "\(restaurant.name.capitalizingFirstLetter())"
        }
    }
}

extension DishViewController: PageViewDelegate {
    
    func setImage(for page: Int) {
        let dish = self.dishes[page]
        let imageView = self.pageView.imageViews[page]
        
        if let urlStr = dish.picture {
            if let url = URL(string: urlStr) {
                imageView.af_setImage(withURL: url)
            }
        }
    }
    func didChanged(_ page: Int) {
        updateDishInfo(page: page)
    }
}






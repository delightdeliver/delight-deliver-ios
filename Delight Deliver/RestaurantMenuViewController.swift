//
//  RestaurantMenuViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 19/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit


protocol RestaurantMenuViewControllerDelegate {
    func didSelectDishes(dishes: [String : [Dish : Int]], totalPrice: Int) -> Void
}

class RestaurantMenuViewController: RGPageViewController {
    override var pagerOrientation: UIPageViewControllerNavigationOrientation {
        get {
            if DeviceType.iPad {
                return .vertical
            }
            
            return .horizontal
        }
    }
    
    override var tabbarPosition: RGTabbarPosition {
        get {
            if DeviceType.iPad {
                return .right
            }
            
            return .top
        }
    }
    
    override var tabbarStyle: RGTabbarStyle {
        get {
            if DeviceType.iPad {
                return .solid
            }
            
            return .blurred
        }
    }
    
    override var tabIndicatorColor: UIColor {
        get {
            return UIColor(red: 0.4902, green: 0.8627, blue: 0.2902, alpha: 1)
        }
    }
    
    override var barTintColor: UIColor? {
        get {
            if DeviceType.iPad {
                return nil
            }
            
            return self.navigationController?.navigationBar.barTintColor
        }
    }
    
    override var tabStyle: RGTabStyle {
        get {
            return .none
        }
    }
    
    override var tabbarWidth: CGFloat {
        get {
            return 140.0
        }
    }
    
    override var tabMargin: CGFloat {
        get {
            return 8.0
        }
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    var restaurant: Restaurant!
    var selectedDishes: [String : [Dish : Int]] = [:]
    var categories: [String]!
    var selectedDishIndex: Int!
    
    
//    var initialIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.reloadData()
        self.datasource = self
        self.delegate = self
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDishFromMenu" {
            let destination = segue.destination as! DishViewController
            destination.dishes = self.restaurant.menu[categories[self.currentPageIndex]]!
            destination.selectedDishIndex = self.selectedDishIndex
        }
    }

}

extension RestaurantMenuViewController: RGPageViewControllerDelegate {
    func widthForTab(at index: Int) -> CGFloat {
        if DeviceType.iPad {
            return tabbarWidth
        } else {
            var tabSize = categories[index].size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17)])
        
            tabSize.width += 32
        
            return tabSize.width
        }
    }
    
    func heightForTab(at index: Int) -> CGFloat {
        return 164.0
    }
    
    func didChangePage(to index: Int) {
        
    }
}

extension RestaurantMenuViewController: RestaurantMenuDataViewControllerDelegate {
    func didSelectDish(dish: Dish, index: Int) {
        self.selectedDishIndex = index
        self.performSegue(withIdentifier: "showDishFromMenu", sender: self)
    }
    
    func didUpdatedDishes(dishes: [Dish : Int], category: String) {
        
    }
}

extension RestaurantMenuViewController: RGPageViewControllerDataSource {
    
    func viewController(for pageViewController: RGPageViewController, at index: Int) -> UIViewController? {
        if (self.restaurant.menu.count == 0) || (index >= self.restaurant.menu.count) {
            return nil
        }
        
        let dataViewController = storyboard!.instantiateViewController(withIdentifier: "RestaurantMenuDataViewController") as! RestaurantMenuDataViewController
        
        let category = self.categories[index]
        
        dataViewController.dishes = self.restaurant.menu[category]!
        dataViewController.category = category
        dataViewController.restaurant = self.restaurant
        dataViewController.delegate = self
        
        return dataViewController
    }
    
    func numberOfPages(for pageViewController: RGPageViewController) -> Int {
        return self.restaurant.menu.count
    }
    
    func tabView(for pageViewController: RGPageViewController, at index: Int) -> UIView {
        let tabView = UILabel()
        tabView.font = UIFont(name: "Acrom", size: 15)!
        
        tabView.textColor = UIColor.black
        tabView.highlightedTextColor = UIColor(red: 0.4902, green: 0.8627, blue: 0.2902, alpha: 1)
        
        tabView.text = categories[index].capitalizingFirstLetter()
        tabView.sizeToFit()
        
        
        return tabView
    }
}

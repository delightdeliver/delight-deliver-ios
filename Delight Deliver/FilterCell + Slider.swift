//
//  FilterCell + Slider.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 26/07/2016.
//  Copyright © 2016 Artem Ryabukha. All rights reserved.
//

import Foundation

enum SliderCellType: String {
    case Calories = "cal"
    case Price = "price"
}


extension FilterCell {
    
    var sliderText: String! {
        get {
            return "cal"
        }
        
    }
    
    func configureSlider() {
        
    }
}

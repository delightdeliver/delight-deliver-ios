//
//  PopupView.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 22/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class PopupView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    var titleString: String? {
        didSet {
            titleLabel.text = titleString!
        }
    }
    
    var delegate: PopupViewDelegate?
    
    func makeAction() {
        delegate?.didPressAction()
    }
    
    func cancelAction() {
        delegate?.didPressCancel()
    }
    
    @IBAction func action(_ sender: AnyObject) {
        makeAction()
    }
    @IBAction func cancel(_ sender: AnyObject) {
        cancelAction()
    }
}

protocol PopupViewDelegate {
    func didPressCancel() -> Void
    func didPressAction() -> Void
}

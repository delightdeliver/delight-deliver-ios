//
//  Restaurant.swift
//  Delight Deliver
//
//  Created by Artem on 26/12/15.
//  Copyright © 2015 temi4hik. All rights reserved.
//

import UIKit
import MapKit

func ==(lhs: Restaurant, rhs: Restaurant) -> Bool {
    return lhs.id == rhs.id
}

class Restaurant: Hashable {
    
    var hashValue: Int {
        get {
            return self.id
        }
    }
    
    var id: Int
    var name: String = "NoName"
    var picture: String? = nil
    var pictureBackstage: String? = nil
    var pictureInterior: String? = nil
    var addresses: [String] = []
    var rating: Int = 0
    var cuisines: [String] = []
    var addressesCoordinates: [CLLocationCoordinate2D] = []
    var deliveryCost: Int? = nil
    var minimumSum: Int? = nil
    var averageBill: Int? = nil
    var workFrom: String? = nil
    var workTo: String? = nil
    var tags: [String] = []
    var categories: [String] = []
    var numberOfVotes: Int? = nil
    var qrDiscount: Int? = nil
    var menu: [String: [Dish]] = [:]
    
    init (restaurantDict: [String:Any]) {
        self.id = restaurantDict["restaurant_ID"] as! Int
        
        if let name = restaurantDict["name"] as? String {
            self.name = name
        }
        if let pictureInterior = restaurantDict["picture_interior"] as? String {
            self.pictureInterior = pictureInterior
        }
        if let picture = restaurantDict["picture"] as? String {
            self.picture = picture
        }
        if let addresses = restaurantDict["addresses"] as? [String] {
            self.addresses = addresses
        }
        if let rating = restaurantDict["rating"] as? Int {
            self.rating = rating
        }
        if let cuisines = restaurantDict["cuisines"] as? [String] {
            self.cuisines = cuisines
        }
        if let addressesGeo = restaurantDict["addresses_geo"] as? [[String:Double]] {
            for address in addressesGeo {
                let lat = address["lat"]
                let lon = address["lon"]
                addressesCoordinates.append(CLLocationCoordinate2D(latitude: lat!, longitude: lon!))
            }
        }
        
        if let categories = restaurantDict["categories"] as? [String] {
            self.categories = categories
        }
        
        if let deliveryCost = restaurantDict["delivery_cost"] as? Int {
            self.deliveryCost = deliveryCost
        }
        if let minimumSum = restaurantDict["minimum_sum"] as? Int {
            self.minimumSum = minimumSum
        }
        if let workFrom = restaurantDict["work_from"] as? String {
            self.workFrom = workFrom
        }
        if let workTo = restaurantDict["work_to"] as? String {
            self.workTo = workTo
        }
        if let tags = restaurantDict["tags"] as? [String] {
            self.tags = tags
        }
        
        if let qrDiscount = restaurantDict["qr_discount"] as? Int {
            self.qrDiscount = qrDiscount
        }
        
        if let numberOfVotes = restaurantDict["number_of_votes"] as? Int {
            self.numberOfVotes = numberOfVotes
        }
        
        RestaurantHelper.instance.addRestaurant(rest: self)
        
        self.loadMenu { }
    }
    
    func loadMenu(success:@escaping() -> Void) {
        NetworkHelper.instance.request(apiMethod: "/api/restaurant_menu/\(self.id)", method: .get, parameters: nil, success: { data, headers,code in
            
            do {
                guard let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any] else {
                    return
                }
                
                for (category, dishes) in json {
                    if let dishArray = dishes as? [[String:Any]] {
                        self.menu[category] = []
                        for dishDict in dishArray {
                            let dishId = dishDict["ID"] as! Int
                            if let dish = DishHelper.instance.getDish(id: dishId) {
                                self.menu[category]!.append(dish)
                                continue
                            }
                            if let dish = Dish(dishDict: dishDict as NSDictionary) {
                                self.menu[category]!.append(dish)
                            }
                        }
                    }
                }
                success()
            } catch { }
            
            }, failure: { error in }
        )
    }
}

class ConcreteRestaurant: NSObject, MKAnnotation {
    
    let restaurant: Restaurant
    let index: Int
    
    @objc var coordinate: CLLocationCoordinate2D {
        return self.restaurant.addressesCoordinates[index]
    }
    
    var title: String? {
        return self.restaurant.name.capitalizingFirstLetter()
    }
    var subtitle: String? {
        var tagsWithSharp: [String] = []
        for tag in self.restaurant.tags {
            tagsWithSharp += ["#\(tag)"]
        }
        return tagsWithSharp.joined(separator: ", ")
    }
    
    init (restaurant: Restaurant, coordinateIndex: Int) {
        self.restaurant = restaurant
        self.index = coordinateIndex
    }
}

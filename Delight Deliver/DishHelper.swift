//
//  DishHelper.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 25/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import Foundation


class DishHelper {
    class var instance : DishHelper {
        struct Singleton {
            static let instance = DishHelper()
        }
        return Singleton.instance
    }
    
    private init() {
        
    }
    
    var dishes: [Dish] = []
    
    func addDish(dish: Dish) {
        
        if self.dishes.contains(dish) {
            return
        }
        dishes.append(dish)
    }
    
    func getDish(id: Int) -> Dish? {
        
        if let idx = self.dishes.index(where: { $0.id == id }) {
            return self.dishes[idx]
        }
        return nil
    }
    
}

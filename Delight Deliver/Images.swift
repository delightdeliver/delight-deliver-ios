//
//  Images.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 19/07/2016.
//  Copyright © 2016 Artem Ryabukha. All rights reserved.
//

import Foundation
import UIKit

struct Images {
    static var waveImage: UIImage {
        get {
            if DeviceType.iPhone5 {
                return UIImage(named: "Wave5S")!
            } else if DeviceType.iPhone6 {
                return UIImage(named: "Wave6")!
            } else if DeviceType.iPhone6Plus {
                return UIImage(named: "Wave6Plus")!
            } else {
                return UIImage(named: "Wave6Plus")!
            }
        }
    }
    
    
    static var arrowUp: UIImage = UIImage(named: "FilterUpArrow")!;
    static var arrowDown: UIImage = UIImage(named: "FilterDownArrow")!;
    
    static var checked: UIImage = UIImage(named: "Checked")!
    static var notChecked: UIImage = UIImage(named: "NotChecked")!
    
    static let mapImage = UIImage(named: "MapCabbage")!
    static let mapImageSelected = UIImage(named: "MapCabbageSelected")!
    
    
    static let location = UIImage(named: "Location")!
    static let workTime = UIImage(named: "WorkTime")!
    
    static let greenCheck = UIImage(named: "GreenCheck")!
    static let profileExit = UIImage(named: "ProfileExit")!
    static let profileAddress = UIImage(named: "ProfileAddress")!
    static let profileEdit = UIImage(named: "ProfileEdit")!
    static let profileStatus = UIImage(named: "ProfileStatus")!
    static let profileOrders = UIImage(named: "ProfileOrders")!
    
    
    
    static let greenDownArrow = UIImage(named: "GreenDownArrow")!
    static let greenUpArrow = UIImage(named: "GreenUpArrow")!
    static let greenLeftArrow = UIImage(named: "GreenLeftArrow")!
    
}

//
//  OrderHistoryCell.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 20/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class OrderHistoryCell: UITableViewCell {
    
    @IBOutlet weak var restautantImageView: UIImageView!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var orderNumberLabel: UILabel!
    
    @IBOutlet weak var orderTimeLabel: UILabel!
    @IBOutlet weak var orderSumLabel: UILabel!
    
    @IBOutlet weak var orderStatusLabel: UILabel!
    
    // Content title outlets
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    
    // Dish outlets
    @IBOutlet weak var dishImageView: UIImageView!
    @IBOutlet weak var dishNameLabel: UILabel!
    @IBOutlet weak var dishDescriptionLabel: UILabel!
    @IBOutlet weak var dishCountLabel: UILabel!
    @IBOutlet weak var dishPriceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if self.orderStatusLabel != nil {
            
            orderStatusLabel.layer.cornerRadius = 3.0
            orderStatusLabel.layer.masksToBounds = true
            orderStatusLabel.layer.borderColor = UIColor.lightGray.cgColor
            orderStatusLabel.layer.borderWidth = 1.0
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func collapse() {
        self.arrowImageView.image = Images.greenDownArrow
    }
    
    override func expand() {
        self.arrowImageView.image = Images.greenUpArrow
    }
}

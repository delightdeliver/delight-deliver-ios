//
//  Address.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 20/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import Foundation


struct Address {
    
    var address: String
    var name: String
    
    
    func toDictionary() -> NSDictionary {
        return [
            "name": self.name,
            "address": self.address,
        ]
    }
}

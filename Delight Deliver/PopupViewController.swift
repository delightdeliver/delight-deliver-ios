//
//  PopupViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 22/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit


class PopupViewController: UIViewController {
    weak var popupView: PopupView!
    var grayView: UIView!
    var tabBarGrayView: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.popupView.setCornerRadius(10)
        
        self.presentingViewController?.tabBarController?.tabBar.addSubview(tabBarGrayView)
        self.presentingViewController?.view.addSubview(grayView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        grayView.removeFromSuperview()
        tabBarGrayView.removeFromSuperview()
    }
   
}

extension PopupViewController: PopupViewDelegate {
    func didPressCancel() {
        self.dismiss(animated: true) {
            
        }
    }
    
    func didPressAction() {
        self.dismiss(animated: true) {
            
        }
    }
}

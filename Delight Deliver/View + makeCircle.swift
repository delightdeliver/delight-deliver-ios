//
//  ImageView + makeCircle.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 21/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit
import UIKit

extension UIView {
    func makeCircle() -> Void {
        self.setCornerRadius(self.frame.width / 2)
    }
    
    func setCornerRadius(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}


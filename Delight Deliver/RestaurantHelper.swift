//
//  RestaurantHelper.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 18/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import Foundation

class RestaurantHelper {
    class var instance : RestaurantHelper {
        struct Singleton {
            static let instance = RestaurantHelper()
        }
        return Singleton.instance
    }
    private init() {
        
    }
    
    var restaurants: [Restaurant] = []
    
    func addRestaurant(rest: Restaurant) {
        
        if self.restaurants.contains(rest) {
            return
        }
        
        restaurants.append(rest)
    }
    
    func getRestaurant(id: Int) -> Restaurant? {
        
        if let idx = self.restaurants.index(where: { $0.id == id }) {
            return self.restaurants[idx]
        }
        return nil
    }
}

//
//  FilterCell + Button.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 26/07/2016.
//  Copyright © 2016 Artem Ryabukha. All rights reserved.
//

import Foundation


extension FilterCell {
    func configureButton() {
        if findButton != nil && activityIndicator != nil {
            // Current cell is ButtonFilterCell
            findButton.layer.cornerRadius = buttonCornerRadius
            findButton.layer.masksToBounds = true
        }
    }
}

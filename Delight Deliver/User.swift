//
//  User.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 17/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import Foundation


class User {
    static var user: User?
    
    static func setUser(email: String, name: String?, surname: String, phone: String) {
        user = User(email: email)
        user?.name = name
        user?.surname = surname
        user?.phone = phone
    }
    
    
    static func setUser(dictionary: [String:Any]) {
        guard let email = dictionary["email"] as? String else {
            return
        }
        
        user = User(email: email)
        
        if let surname = dictionary["surname"] as? String {
            user!.surname = surname
        }
        
        if let name = dictionary["name"] as? String {
            user!.name = name
        }
        if let phone = dictionary["phone"] as? String {
            user!.phone = phone
        }
        
        if let addressArray = dictionary["addresses"] as? [[String:Any]] {
            for addressDict in addressArray {
                let name = addressDict["name"] as! String
                let address = addressDict["address"] as! String
                user?.addresses.append(Address(address: address, name: name))
            }
        }
    }
    
    static func removeUser() {
        user = nil
    }
    
    private init(email: String) {
        self.email = email
    }
    
    var email: String
    var name: String?
    var surname: String?
    var phone: String?
    var addresses: [Address] = []
    
}

//
//  ProfileAddressesViewController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 20/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import UIKit

class ProfileAddressesViewController: UITableViewController {
    
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var addAddressButton: UIButton!
    
    
    var user: User {
        get {
            return User.user!
        }
    }
    
    var addresses: [Address] {
        get {
            return user.addresses
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ProfileAddressCell", bundle: nil), forCellReuseIdentifier: "ProfileAddressCell")
        
        print(tableView.frame.height)
        self.tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Cell.heightes["ProfileAddressCell"]!
    }
    
    @IBAction func addAddress(_ sender: AnyObject) {
        
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 80
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addresses.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let currentCellId = "ProfileAddressCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: currentCellId, for: indexPath) as! ProfileAddressCell
        
        
        cell.addressLabel.text = addresses[indexPath.row].address
        cell.addressNameLabel.text = addresses[indexPath.row].name
        
        
        return cell
    }
    

}

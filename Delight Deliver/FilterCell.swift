//
//  FilterCell.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 19/07/2016.
//  Copyright © 2016 Artem Ryabukha. All rights reserved.
//

import UIKit

protocol FilterCellDelegate {
    
    func didChangeRegularValue(newValues: [String], parentCell: FilterCell)
    
    func didChangeSliderValue(newSliderValue: Int, parentCell: FilterCell)
    
    func didToggleTag(newTags: Set<String>, parentCell: FilterCell)
    
    func didChooseDish(parentCell: FilterCell)
    
    func didChooseRestaurant(parentCell: FilterCell)
    
    func didPressFindButton(parentCell: FilterCell)
}


class FilterCell: UITableViewCell {
    
    let topCornerRadius: CGFloat = 20.0
    let buttonCornerRadius: CGFloat = 10.0
    
    
    // MARK: TopFilterCell outlets
    @IBOutlet weak var dishButton: UIButton!
    @IBOutlet weak var restaurantButton: UIButton!
    @IBOutlet weak var wavesImageView: UIImageView!
    @IBOutlet weak var searchLabel: UILabel!
    
    
    // MARK: TitleFilterCell outlets
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    
    // MARK: RegularFilterCell outlets
    @IBOutlet weak var regularTitleLabel: UILabel!
    @IBOutlet weak var regularSelectedLabel: UILabel!
    
    
    // MARK: RegularSelectionCell outlets
    @IBOutlet weak var selectionTableView: UITableView!
    var items: [String] = []
    var selectedIndexes: [Int] = []
    var selectedItem: String!
    
    
    
    // MARK: SliderFilterCell outlets
    @IBOutlet weak var sliderTitleLabel: UILabel!
    @IBOutlet weak var sliderFromLabel: UILabel!
    @IBOutlet weak var sliderToLabel: UILabel!
    @IBOutlet weak var sliderCurrentLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    // MARK: ButtonFilterCell outlets
    @IBOutlet weak var findButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var textCellType: TextCellType!
    var sliderCellType: SliderCellType!
    
    
    var isFirstPerson: Bool?
    
    @IBAction func chooseDish(_ sender: AnyObject) {
        guard delegate != nil else {
            return
        }
        
        delegate.didChooseDish(parentCell: self)
    }
    
    @IBAction func sliderChanged(_ sender: AnyObject) {
        guard delegate != nil else {
            return
        }
        self.sliderCurrentLabel.text = "\(Int(self.slider.value))"
        delegate.didChangeSliderValue(newSliderValue: Int(self.slider.value), parentCell: self)
    }
    
    @IBAction func chooseRestraurant(_ sender: AnyObject) {
        guard delegate != nil else {
            return
        }
        
        delegate.didChooseRestaurant(parentCell: self)
    }
    
    var tagsControl: TagsControl!
    
    
    var delegate: FilterCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        configureTop()
        configureButton()
        configureTags()
        configureSlider()
        
        
        if selectionTableView != nil {
            selectionTableView.dataSource = self
            selectionTableView.delegate = self
            selectionTableView.bounces = false
            selectionTableView.tableHeaderView = nil
        }
    }
    @IBAction func find(_ sender: AnyObject) {
        
        if delegate != nil {
            delegate.didPressFindButton(parentCell: self)
        }
    }
    
    
}

extension FilterCell: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.textCellType! == .Ingredients {
            selectedIndexes.append(indexPath.row)
            updateSelected()
        } else {
            self.selectedItem = items[indexPath.row]
            if delegate != nil {
                delegate.didChangeRegularValue(newValues: [self.selectedItem], parentCell: self)
            }
        }
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if self.textCellType! == .Ingredients {
            selectedIndexes.remove(object: indexPath.row)
            updateSelected()
        }
    }
    func updateSelected() {
        var selected: [String] = []
        
        for idx in selectedIndexes {
            selected.append(items[idx])
        }
        if delegate != nil {
            delegate.didChangeRegularValue(newValues: selected, parentCell: self)
        }
    }
    
}

extension FilterCell: UITableViewDataSource {
    
    
    @objc(tableView:heightForRowAtIndexPath:) func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier = "RegularItemFilterCell"
        
        tableView.register(UINib(nibName: "RegularItemFilterCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: tableView.bounds.width, height: 0.01))
        
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? FilterItemCell
        
        if cell == nil {
            cell = Bundle.main.loadNibNamed(reuseIdentifier, owner: self, options: nil)![0] as? FilterItemCell
        }
        
        cell?.titleLabel.text = items[indexPath.row]
        
        if self.textCellType! == .Ingredients {
            cell?.setSelected(selectedIndexes.contains(indexPath.row), animated: false)
        } else {
            if self.selectedItem != nil {
                if selectedItem == items[indexPath.row] {
                    cell?.setSelected(true, animated: false)
                }
            }
        }
        
        
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}


extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}



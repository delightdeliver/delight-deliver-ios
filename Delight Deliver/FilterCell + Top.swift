//
//  FilterCell + Top.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 26/07/2016.
//  Copyright © 2016 Artem Ryabukha. All rights reserved.
//

import Foundation


extension FilterCell {
    func configureTop() {
        if dishButton != nil && restaurantButton != nil && wavesImageView != nil {
            // Current cell is TopFilterCell
            dishButton.layer.cornerRadius = topCornerRadius
            restaurantButton.layer.cornerRadius = topCornerRadius;
            
            dishButton.layer.masksToBounds = true
            restaurantButton.layer.masksToBounds = true
            
            wavesImageView.image = Images.waveImage
        }
    }
    
    
}

//
//  PopupController.swift
//  Delight Deliver
//
//  Created by Artem Ryabukha on 22/10/2016.
//  Copyright © 2016 Prityko Polina. All rights reserved.
//

import Foundation
import UIKit

enum PopupType {
    case rateDish, rateRestaurant
    case restaurantClosed
    case clearBasket
    case dishFilter
    case qr
    
    var nibName: String {
        switch self {
        case .rateDish, .rateRestaurant:
            return "RatePopup"
        default:
            return ""
        }
    }
    var size: CGSize {
        
        switch self {
        case .rateDish, .rateRestaurant:
            return CGSize(width: 300, height: 340)
        default:
            return CGSize.zero
        }
    }
}


class PopupController {
    
    var popupController: PopupViewController
    var popupView: PopupView
    
    init(type: PopupType) {
        self.popupController = PopupViewController()
        let clearView = UIView(frame: UIScreen.main.bounds)
        clearView.backgroundColor = UIColor.clear
        popupController.view = clearView
        popupController.view.isOpaque = false
        
        
        let grayView = UIView(frame: UIScreen.main.bounds)
        grayView.backgroundColor = UIColor(white: 0.2, alpha: 0.5)
        let tabBarGrayView = UIView(frame: grayView.frame)
        tabBarGrayView.backgroundColor = grayView.backgroundColor
        popupController.grayView = grayView
        popupController.tabBarGrayView = tabBarGrayView
        
        self.popupView = Bundle.main.loadNibNamed(type.nibName, owner: nil, options: nil)?[0] as! PopupView
        popupController.popupView = popupView
        popupController.view.addSubview(popupView)
        
        popupView.delegate = popupController
        
        popupController.modalTransitionStyle = .crossDissolve
        popupController.modalPresentationStyle = .overCurrentContext
        
        popupView.center = CGPoint(x: clearView.frame.width / 2, y: clearView.frame.height / 2)
    }
    
    func showPopup(viewController: UIViewController) {
        viewController.present(popupController, animated: true, completion: nil)
    }
}



